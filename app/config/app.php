<?php  defined('BASEPATH')  OR  exit('No direct script access allowed');

$config['site_title']   = 'Health Minister Schedule'; //Site Title Goes Here
$config['site_main']    = ''; //Site Main Url Goes Here

$config['adminEmail']   = 'eftakhairul@rightbrainsolution.com'; //Admin Email Address
$config['infoEmail']    = ''; // Info Email Address
$config['infoName']     = ''; // Info Name
$config['rowsPerPage'] = 20;


/*     Configurations of Attendances     */
$config['designationStatuses'] = array( 1 => 'Regular', 2 => 'Current Charge', 3 => 'In-Charge',
    4 => 'Working Deputation', 5 => 'Study Deputation', 6 => 'OSD', 7 => 'Lien');
$config['job_class'] = array(1 => '1st', 2 => '2nd', 3 =>'3rd', 4 => '4th');
$config['marital_status'] = array(1 => 'Single', 2 => 'Married', 3 =>'Divorced', 4 => 'Widowed', 5 =>'Separated');
$config['religion'] = array(1 => 'Muslim', 2 => 'Hindu', 3 =>'Christian', 4 => 'Buddhist', 5 => 'Other');
$config['event'] = array(1 => 'Transferred', 2 => 'Terminated', 3 =>'Suspended', 4 => 'Resigned', 5 =>'Died');


/*     Configurations of Schedules     */
$config['schedule_statuses'] = array(
    'completed' => 'Completed Schedules',
    'cancelled' => 'Cancelled Schedules',
    'postponed' => 'Postponed Schedules',
);

$config['grace'] = array(
    1 => 'Chief Guest',
    2 => 'Special Guest',
    3 => 'Guest of Honor',
    4 => 'Chair',
    5 => 'Participant',
    6 => 'Not Applicable'
);