<?php

/**
 * Description of Schedules Controller
 *
 * @package     Controller
 * @author      Eftakhairul <eftakhairul@gmail.com>
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class SchedulesController extends BaseController
{
    public function __construct ()
    {
        parent::__construct();
        $this->ensureLoggedIn();

        $this->load->library('pagination');
        $this->load->model('schedule');
        $this->load->helper('date');
        $this->data['userType'] = $this->session->userdata('userType');
    }

    public function index()
    {
        $this->load->model('group');
        $uriAssoc = $this->uri->uri_to_assoc();
        $uriAssoc = array_merge($uriAssoc, array(
                        'url' => site_url('schedules/schedules/index'),
                        'status' => 'due'
                   ));

        $this->processPagination($uriAssoc);
        $this->data['flagForPrint'] = true;
        $this->data['refreshFlag'] = 1;
        $this->data['groups'] = $this->group->getAll();

        $this->layout->view('schedules/schedules/index', $this->data);
    }

    public function printSchedule()
    {
        $this->load->model('group');
        $this->load->helper('table_row');
        $options['group_id'] = $this->session->userdata('groupType');
        $this->data['schedules'] = $this->sortDataAsGroupByDate($this->schedule->getAllForPrint($options));
        $this->data['title'] = $this->group->getPrintTitle($this->session->userdata('groupType'));

        $this->load->view('schedules/schedules/print-schedule', $this->data);
    }

    private function sortDataAsGroupByDate($data)
    {
        $schedules = array();

        foreach($data AS $row) {
            $schedules[$row['date']][] = $row;
        }

        return $schedules;
    }

    public function viewAll()
    {
        $uriAssoc = $this->uri->uri_to_assoc();
        $uriAssoc = array_merge($uriAssoc, array(
                        'url' => site_url('schedules/schedules/viewAll'),
                        'isPast' => true
                   ));
        $this->processPagination($uriAssoc);

        $this->layout->view('schedules/schedules/index', $this->data);
    }

    private function processPagination($options)
    {
        $this->load->library('pagination');

        $options['page'] = empty ($options['page']) ? 0 : $options['page'];

        if ($this->session->userdata('userType') != SUPER_ADMIN) {
            $options['group_id'] = $this->session->userdata('groupType');
        }

        $this->data['schedules'] = $this->schedule->getAll($options);

        $paginationOptions = array(
            'baseUrl' => $options['url'] . (empty ($options['status']) ? '' : '/status/'.$options['status']). '/page/',
            'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
            'numRows' => $this->schedule->countAllSchedules($options)
        );

        $this->pagination->setOptions($paginationOptions);
    }

    public function create()
    {
        $this->checkAdmin();

        $this->load->model('status');
        $this->load->library('form_validation');
        $this->form_validation->setRulesForCreateSchedule();

        if (!empty ($_POST)) {
            
            if ($this->form_validation->run()) {

                $_POST['group_id'] = $this->session->userdata('groupType');

                if ($this->schedule->save($_POST)) {
                     $this->redirectForSuccess('schedules', 'Event has been created successfully');
                } else {
                    $this->data['error'] = 'Data is not saved.';
                }

            } else {
                $this->data['error'] = 'Enter required information.';
            }
        }

        $this->data['statuses'] = $this->status->getAll();
        $this->layout->view('schedules/schedules/create', $this->data);
    }

    public function edit($id)
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForCreateSchedule();

        $this->load->model('status');
        $this->data['statuses'] = $this->status->getAll();

        if (!empty ($_POST)) {

            if ($this->form_validation->run()) {

                $_POST['group_id'] = $this->session->userdata('groupType');
                if ($this->schedule->update($_POST, $id)) {
                     $this->redirectForSuccess('schedules/schedules', 'Event has been updated successfully');
                } else {
                    $this->data['error'] = 'Data is not save';
                }

            } else {
                $this->data['error'] = 'Enter required information.';
                $this->data['schedules'] = $_POST;
            }

        } else {
            $this->data['schedules'] = $this->schedule->getAllById($id);
        }

        $this->layout->view('schedules/schedules/edit', $this->data);
    }

    public function deleteEvent()
    {
        if (!$this->checkAdmin()) {
            return;
        }

        $data = $this->uri->uri_to_assoc();

        if (empty ($data['id'])) {
            $this->redirectForFailure('schedules/schedules', 'Event is not found');
        } else {
            $status = array('status_id' => 3);
            $this->schedule->update($status, $data['id']);
            $this->redirectForSuccess('schedules/schedules', 'Event is deleted successfully');
        }
    }

    public function viewEvent($id)
    {
        if (empty($id)) {
            $this->redirectForFailure('schedules/schedules', 'Date is not found');
        }

        $this->data['schedule'] = $this->schedule->getDetailById($id);
        $this->layout->view('schedules/view-event', $this->data);
    }

    public function searchByGroup()
    {
        $this->load->model('group');
        $uriAssoc = $this->uri->uri_to_assoc();
        $uriAssoc = array_merge($uriAssoc, array(
                        'url' => site_url('schedules/schedules/index'),
                        'status' => 'due'
                   ));
        if (!empty ($_POST)) {
             $uriAssoc['group_id'] = $_POST['group_id'];
         }

        $this->processPagination($uriAssoc);
        $this->data['groups'] = $this->group->getAll();

        $this->layout->view('schedules/search-by-group-name', $this->data);
    }

    public function pastSchedule()
    {
        $this->load->model('group');
        $options['group_id'] = $this->session->userdata('groupType');

        if(!empty($_POST)) {
            $this->data['schedules'] = $this->schedule->pastschedule($options, $_POST);
        } else {
            $this->data['schedules'] = $this->schedule->pastschedule($options);
        }

        $this->load->helper('date');
        $this->layout->view('schedules/schedules/past-schedule', $this->data);
    }

    protected function checkAdmin()
    {
        if ($this->session->userdata('userType') != SUPER_ADMIN AND $this->session->userdata('userType') != ADMIN) {
            $this->redirectForFailure('schedules', 'You are not authorized for this section.');
            return false;
        }
        return true;
    }
}