<?php

/**
 * Description of Users Controller
 *
 * @package     Controller
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 * @author      Eftakhairul Islam <eftakhairul@gmail.com>
 */
class UsersController extends BaseController
{
    public function __construct ()
    {
        parent::__construct();
        $this->load->model('user');
        $this->data['userType'] = $this->session->userdata('userType');
    }

    public function index()
    {
        if ($this->session->userdata('userType')) {
            redirect('schedules/users/manage');
        }

        $this->load->library('form_validation');
        $this->form_validation->setRulesForSignIn();

        if (!empty ($_POST)) {

            if ($this->form_validation->run()) {
                
                $result = $this->user->validateUser($_POST);
                if ($result) {

                    $this->session->set_userdata('username', $result['username']);
                    $this->session->set_userdata('userType', $result['user_type_id']);
                    $this->session->set_userdata('groupType', $result['group_id']);
                    $this->session->set_userdata('user_id', $result['user_id']);
                    $this->redirectForSuccess('schedules/schedules', 'You have successfully logged in.');

                } else {
                    $this->data['error'] = 'Enter correct Username & Password.';
                }
                
            } else {
                $this->data['error'] = 'Enter required information.';
            }
        }

        $this->load->view('schedules/users/index', $this->data);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('schedules/users');
    }

    public function manage()
    {
        $this->checkAdmin();

        $url = site_url('schedules/users/manage');
        $this->processPagination($url);

        $this->layout->view('schedules/users/manage', $this->data);
    }

    private function processPagination($url)
    {
        $this->load->library('pagination');

        $uriAssoc = $this->uri->uri_to_assoc();
        $page = empty ($uriAssoc['page']) ? 0 : $uriAssoc['page'];
        $this->data['users'] = $this->user->getAll($page);

        $paginationOptions = array(
            'baseUrl' => $url . '/page/',
            'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
            'numRows' => $this->user->countAllUsers()
        );

        $this->pagination->setOptions($paginationOptions);
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->load->model('group');
        $this->form_validation->setRulesForUserEntry();

        if (!empty ($_POST)) {

            if ($this->form_validation->run()) {

                $_POST['user_id'] = $this->user->save($_POST);
                $this->load->model('profile');
                $this->profile->save($_POST);
                $this->redirectForSuccess('schedules/users/manage',
                       'The user information has been created successfully.');

            } else {
                $this->data['errorMessage'] = 'Please correct the following errors.';
            }
        }

        $this->data['types'] = $this->user->getUserTypes();
        $this->data['groups'] = $this->group->getAll();

        $this->layout->view('schedules/users/add', $this->data);
    }

    public function edit()
    {
        $this->load->library('form_validation');
        $this->load->model('group');
        $this->form_validation->setRulesForUserEntry(array('isEdit' => true, 'userType' => $this->session->userdata('userType')));

        if (!empty ($_POST)) {

            if ($this->form_validation->run()) {

                $this->user->modify($_POST);
                $this->load->model('profile');
                $this->profile->modify($_POST);
                $this->redirectForSuccess('schedules/users/manage',
                       'The user information has been updated successfully.');

            } else {
                $this->data['errorMessage'] = 'Please correct the following errors.';
                $this->data['user'] = $_POST;
            }
        } else {
            $data = $this->uri->uri_to_assoc();

            if (empty ($data['id'])) {
                $this->redirectForFailure('schedules/users/manage', 'User id has not found.');
            } else {

                if (($this->session->userdata('userType') != SUPER_ADMIN) AND ($this->session->userdata('user_id') != $data['id'])) {
                    $this->redirectForFailure('schedules', 'You do not have the permission.');
                }

                $this->data['user'] = $this->user->getDetail($data['id']);

                if (empty ($this->data['user'])) {
                    $this->redirectForFailure('schedules/users/manage','User data has not found.');
                }var_dump($this->data['user'], $this->user->getLastQuery());
            }
        }

        $this->data['types'] = $this->user->getUserTypes();
        $this->data['groups'] = $this->group->getAll();

        $this->layout->view('schedules/users/edit', $this->data);
    }

    public function delete()
    {
        if (!$this->checkAdmin()) {
            return;
        }

        $data = $this->uri->uri_to_assoc();

        if (empty ($data['id'])) {
            $this->redirectForFailure('schedules/users/manage', 'User id has not found.');
        } else {
            $this->user->remove($data['id']);
            $this->redirectForSuccess('schedules/users/manage', 'Deletion is successful.');
        }
    }

    public function username_check($username)
    {
        return !$this->user->checkUsernameExisted($username);
    }

    protected function checkAdmin()
    {
        if ($this->session->userdata('userType') != SUPER_ADMIN) {
            $this->redirectForFailure('schedules', 'You are not authorized for this section.');
            return false;
        }

        return true;
    }

    public function changePassword()
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForChangePassword();

        if (!empty ($_POST)) {

            if ($this->form_validation->run()) {

                $this->user->modify($_POST);
                $this->redirectForSuccess('schedules', 'The user information has been updated successfully.');

            } else {
                $this->data['errorMessage'] = 'Please correct the following errors.';
                $this->data['user'] = $_POST;
            }
        } else {
            $data = $this->uri->uri_to_assoc();

            if (empty ($data['id'])) {
                $this->redirectForFailure('schedules/users/manage', 'User id has not found.');
            } else {

                if (($this->session->userdata('userType') != SUPER_ADMIN) AND ($this->session->userdata('user_id') != $data['id'])) {
                    $this->redirectForFailure('schedules/schedules', 'You do not have the permission.');
                }

                $this->data['user'] = array('user_id' => $data['id']);
            }

        }

        $this->layout->view('schedules/users/change-password', $this->data);
    }
}