<?php

/**
 * Description of Meetings Controller
 *
 * @package     Controller
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class MeetingsController extends BaseController
{
    public function __construct ()
    {
        parent::__construct();
        $this->ensureLoggedIn();

        $this->data['userType'] = $this->session->userdata('userType');
        $this->load->model('venue_meeting');
    }
    
    public function index()
    {
        if (empty($_POST['venue_id'])) {
            $this->session->unset_userdata('venue_id');
        } else {
            $this->session->set_userdata('venue_id', $this->input->post('venue_id'));
        }
        
        $url = site_url('schedules/meetings');
        $this->processPagination($url);

        $this->data['refreshFlag'] = 1;
        $this->load->model('venue');
        $this->data['venues'] = $this->venue->getAll();
        $this->layout->view('schedules/meetings/index', $this->data);
    }

    private function processPagination($url)
    {
        $this->load->library('pagination');

        $uriAssoc = $this->uri->uri_to_assoc();
        $page = empty ($uriAssoc['page']) ? 0 : $uriAssoc['page'];
        $this->data['meetings'] = $this->venue_meeting->getAll($page);

        $paginationOptions = array(
            'baseUrl' => $url . '/page/',
            'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
            'numRows' => $this->venue_meeting->countAllGroups()
        );

        $this->pagination->setOptions($paginationOptions);
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->load->model('venue_meeting');
        $this->form_validation->setRulesForAddMeeting();

        if (!empty ($_POST)) {

            if ($this->form_validation->run()) {

                if ($this->venue_meeting->CheckMeeting($_POST)) {
                    $this->data['errorMessage'] = 'schedules is already there';
                } else {
                    $this->venue_meeting->save($_POST);
                    $this->redirectForSuccess('schedules/meetings',
                           'A meeting of a venue has been created successfully.');
                }
            } else {
                $this->data['errorMessage'] = 'Please correct the following errors.';
            }
        }

        $this->load->model('venue');
        $this->load->model('group');
        $this->data['venues'] = $this->venue->getAll();
        $this->data['groups'] = $this->group->getAll();
        $this->layout->view('schedules/meetings/add', $this->data);
    }

    public function edit($id)
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForAddMeeting();

        if (!empty ($_POST)) {

            if ($this->form_validation->run()) {

                if ($this->venue_meeting->CheckMeeting($_POST)) {
                    $this->data['errorMessage'] = 'Schedule is already there';
                } else {

                    if ($this->venue_meeting->modify($_POST)) {
                        $this->redirectForSuccess('schedules/meetings', 'Meeting has been updated successfully');
                    } else {
                        $this->data['error'] = 'Data has not saved';
                    }
                }

                $this->data['meeting'] = $_POST;

            } else {
                $this->data['error'] = 'Enter required information.';
                $this->data['meeting'] = $_POST;
            }

        } else {
            $result = $this->venue_meeting->getAllById($id);
            $this->data['meeting'] = $this->processingDateTime($result);
            var_dump($this->data['meeting']);
        }

        $this->load->model('venue');
        $this->load->model('group');
        $this->data['venues'] = $this->venue->getAll();
        $this->data['groups'] = $this->group->getAll();
        $this->layout->view('schedules/meetings/edit', $this->data);
    }

    public function delete()
    {
        $data = $this->uri->uri_to_assoc();

        if (empty ($data['id'])) {
            $this->redirectForFailure('schedules/venues', 'Venue id has not found.');
        } else {
            $data['is_cancelled'] = 1;
            $this->venue_meeting->update($data, $data['id']);
            $this->redirectForSuccess('schedules/venues', 'Venue deletion has been successful.');
        }
    }

    public function view()
    {
         $data = $this->uri->uri_to_assoc();

        if (empty ($data['id'])) {
            $this->redirectForFailure('schedules/meetings', 'meeting is not found');
        } else {
            $result = $this->venue_meeting->getDetailByMeetingId($data['id']);
            $this->data['meeting'] = $this->processingDateTime($result);
            $this->layout->view('schedules/meetings/view', $this->data);
        }
    }

    public function printMeeting()
    {
        $this->load->helper('date');

        $this->load->model('venue_meeting');
        $this->data['meetings'] = $this->venue_meeting->printAll();
        $this->load->view('schedules/meetings/print-meeting', $this->data);
    }

    private function processingDateTime($data)
    {
        $CI = & get_instance();
        $CI->load->helper('date');

        if(empty($data)) $this->redirectForFailure('schedules/meetings', 'No data has been found.');
        $starting = explode(" ",$data['starting_date_time']);
        $ending = explode(" ",$data['ending_date_time']);
        $data['startingDate'] = mysql_to_human($starting[0]);
        $data['startingTime'] = date("g:i a", strtotime($starting[1]));
        $data['endingDate'] = mysql_to_human($ending[0]);
        $data['endingTime'] = date("g:i a", strtotime($ending[1]));

        return $data;
    }
}