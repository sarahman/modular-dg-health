<?php

/**
 * Description of Venues Controller
 *
 * @package     Controller
 * @author      Eftakhairul <eftakhairul@gmail.com>
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */

class VenuesController extends BaseController
{
    public function __construct ()
    {
        parent::__construct();
        $this->ensureLoggedIn();

        $this->data['userType'] = $this->session->userdata('userType');
        $this->load->model('venue');
    }
    
    public function index()
    {
        $this->checkAdmin();

        $url = site_url('schedules/venues');
        $this->processPagination($url);

        $this->layout->view('schedules/venues/index', $this->data);
    }

    private function processPagination($url)
    {
        $this->load->library('pagination');

        $uriAssoc = $this->uri->uri_to_assoc();
        $page = empty ($uriAssoc['page']) ? 0 : $uriAssoc['page'];
        $this->data['venues'] = $this->venue->getAll($page);

        $paginationOptions = array(
            'baseUrl' => $url . '/page/',
            'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
            'numRows' => $this->venue->countAllGroups()
        );

        $this->pagination->setOptions($paginationOptions);
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForAddVenue();
        
        if (!empty ($_POST)) {

            if ($this->form_validation->run()) {

                $this->venue->save($_POST);
                $this->redirectForSuccess('schedules/venues', 'The venue has been created successfully.');

            } else {
                $this->data['errorMessage'] = 'Please correct the following errors.';
            }
        }

        $this->layout->view('schedules/venues/add', $this->data);
    }

    public function edit($id)
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForAddVenue();

        if (!empty ($_POST)) {

            if ($this->form_validation->run()) {

                if ($this->venue->update($_POST, $id)) {
                     $this->redirectForSuccess('schedules/venues', 'Venue has been updated successfully');
                } else {
                    $this->data['error'] = 'Data is not save';
                }

            } else {
                $this->data['error'] = 'Enter required information.';
                $this->data['venues'] = $_POST;
            }

        } else {
            $this->data['venues'] = $this->venue->getAllById($id);
        }

        $this->layout->view('schedules/venues/edit', $this->data);
    }

    public function delete()
    {
        if (!$this->checkAdmin()) {
            return;
        }

        $data = $this->uri->uri_to_assoc();

        if (empty ($data['id'])) {
            $this->redirectForFailure('schedules/venues', 'Venue id is not found.');
        } else {
            $this->venue->remove($data['id']);
            $this->redirectForSuccess('schedules/venues', 'Venue deletion has been successful.');
        }
    }

    protected function checkAdmin()
    {
        if ($this->session->userdata('userType') != SUPER_ADMIN) {
            $this->redirectForFailure('schedules/users/manage', 'You are not authorized for this section.');
            return false;
        }

        return true;
    }
}