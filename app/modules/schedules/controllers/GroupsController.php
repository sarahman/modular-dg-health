<?php

/**
 * Description of Groups Controller
 *
 * @package     Controller
 * @author      Eftakhairul Islam <eftakhairul@gmail.com>
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class GroupsController extends BaseController
{
    public function __construct ()
    {
        parent::__construct();
        $this->ensureLoggedIn();

        $this->load->library('pagination');
        $this->load->model('group');
        $this->data['userType'] = $this->session->userdata('userType');
    }

    public function index()
    {
        $this->checkAdmin();

        $url = site_url('schedules/groups');
        $this->processPagination($url);

        $this->layout->view('schedules/groups/index', $this->data);
    }

    private function processPagination($url)
    {
        $this->load->library('pagination');

        $uriAssoc = $this->uri->uri_to_assoc();
        $page = empty ($uriAssoc['page']) ? 0 : $uriAssoc['page'];
        $this->data['groups'] = $this->group->getAll($page);

        $paginationOptions = array(
            'baseUrl' => $url . '/page/',
            'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
            'numRows' => $this->group->countAllGroups()
        );

        $this->pagination->setOptions($paginationOptions);
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForAddGroup();

        if (!empty ($_POST)) {

            if ($this->form_validation->run()) {

                $this->group->save($_POST);
                $this->redirectForSuccess('schedules/groups', 'The group has been created successfully.');

            } else {
                $this->data['errorMessage'] = 'Please correct the following errors.';
            }
        }

        $this->layout->view('schedules/groups/add', $this->data);
    }

    public function edit($id)
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForAddGroup();

        if (!empty ($_POST)) {

            if ($this->form_validation->run()) {

                if ($this->group->update($_POST, $id)) {
                    $this->redirectForSuccess('schedules/groups', 'Group has been updated successfully');
                } else {
                    $this->data['error'] = 'Data is not save';
                }

            } else {
                $this->data['error'] = 'Enter required information.';
                $this->data['groups'] = $_POST;
            }

        } else {
            $this->data['group'] = $this->group->getAllById($id);
        }

        $this->layout->view('schedules/groups/edit', $this->data);
    }

    public function delete()
    {
        if (!$this->checkAdmin()) {
            return;
        }

        $data = $this->uri->uri_to_assoc();

        if (empty ($data['id'])) {
            $this->redirectForFailure('schedules/groups', 'User id has not found.');
        } else {
            $this->group->remove($data['id']);
            $this->redirectForSuccess('schedules/groups', 'Deletion is successful.');
        }
    }


    protected function checkAdmin()
    {
        if ($this->session->userdata('userType') != SUPER_ADMIN) {

            $this->redirectForFailure('schedules/users/manage',
                'You are not authorized for this section.'
            );

            return false;
        }

        return true;
    }
}
