SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


INSERT INTO `grace_statuses` (`grace_status_id`, `title`) VALUES
  (1, 'Not applicable'), (2, 'Chief Guest'), (3, 'Special Guest'),
  (4, 'Guest of Honor'), (5, 'Chair'), (6, 'other');

INSERT INTO `groups` (`group_id`, `associated_name`, `print_title`) VALUES
  (NULL, 'DGHS', 'Bangladesh Govt. Medical Institute'),
  (NULL, 'Minister', 'Bangladesh Health Minister');

INSERT INTO `statuses` (`status_id`, `title`) VALUES
  (NULL, 'due'), (NULL, 'completed'), (NULL, 'cancelled'), (NULL, 'postponed');

INSERT INTO `users` (`user_id`, `username`, `password`, `user_type_id`, `created_date`, `modified_date`, `group_id`) VALUES
  (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 2, '2011-08-03', '0000-00-00 00:00:00', 0),
  (2, 'user', 'e10adc3949ba59abbe56e057f20f883e', 3, '2011-08-03', '0000-00-00 00:00:00', 0);

INSERT INTO `venues` (`venue_id`, `title`) VALUES (NULL, 'DGHS'), (NULL, 'Minister Office');

INSERT INTO `user_types` (`user_type_id`, `title`) VALUES (1, 'admin'), (2, 'user'), (3, 'visitor');;


SET FOREIGN_KEY_CHECKS=1;