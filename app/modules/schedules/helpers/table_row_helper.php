<?php

if (!function_exists('makeTableRow')) {
    
    function makeTableRow($date, $schedules)
    {
        $CI = & get_instance();
        $CI->load->helper('date');

        $output = "<tr>
                       <td class='centered' align='center' rowspan='"
                       . count($schedules) . "'>" . mysql_to_human($date)
                       . '<br/>' . date('l', strtotime($date)) . '</td>';

        foreach($schedules AS $schedule) :

            $output .= '<td>' . date("g:i a", strtotime($schedule['time'])) . '</td>';
            $output .= '<td>' . $schedule['title'] . ' To grace as: ' . $schedule['grace'] . '.';
            $output .= empty ($schedule['description']) ? '' : ($schedule['description'] . '.');
            $output .= '<span style="font-weight:bold">';
            if ($schedule['is_date_not_confirmed'] == 1) {
                if ($schedule['is_time_not_confirmed'] == 1) {
                    $output .= "(Date and Time are not confirmed)";
                } else {
                    $output .= "(Date is not confirmed)";
                }
            } else if ($schedule['is_time_not_confirmed'] == 1){
                $output .= "(Time is not confirmed)";
            }

            $output .= "</span>
                        </td>
                        <td>" . $schedule['venue'] ."</td>
                </tr>
                <tr>";

        endforeach;

        $output = substr($output, 0, strlen($output) - 4);

        return $output;
    }
}