<?php

/**
 * Facility Designation Model
 * 
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 * @package     DGHEALTH
 */
class Facility_designation extends MY_Model
{
    public function __construct ()
    {
        parent::__construct();
        $this->loadTable('facilities_designations', 'facility_designation_id');
    }

    public function getFacilityPosts($facilityId)
    {
        $facilities = 'facilities';
        $designations = 'designations';
        $fields = "{$this->table}.*, `{$designations}`.`title` AS `designation`";
        $this->db->select($fields);
        $this->db->from($facilities);
        $this->db->join("{$this->table}",
               "{$facilities}.facility_id = {$this->table}.facility_id");
        $this->db->join("{$designations}",
               "{$this->table}.designation_id = {$designations}.designation_id");
        $this->db->where("{$this->table}.`facility_id`", $facilityId);

        return $this->db->get()->result_array();
    }

    public function getFacilityPostsDetail($facilityId)
    {
        $departments = 'departments';
        $categories = 'categories';
        $designations = 'designations';
        $fields = "{$this->table}.{$this->primaryKey},
                   {$departments}.name AS department,
                   {$categories}.title AS category,
                   {$designations}.title AS designation,
                   {$this->table}.no_of_sanctioned_posts";

        $this->db->select($fields);
        $this->db->from($this->table);
        $this->db->join("{$departments}",
               "{$departments}.department_id = {$this->table}.department_id");
        $this->db->join("{$designations}",
               "{$designations}.designation_id = {$this->table}.designation_id");
        $this->db->join("{$categories}",
               "{$designations}.category_id = {$categories}.category_id");
        $this->db->where("{$this->table}.facility_id", $facilityId);
        
        return $this->db->get()->result_array();
    }

    public function checkDataEntered(array $data)
    {
        $result = $this->find(array(
            'facility_id' => $data['facility_id'],
            'department_id' =>  $data['department_id'],
            'designation_id' =>  $data['designation_id']
        ), $this->primaryKey);

        return !empty($result);
    }

    public function save(array $data)
    {
        $data['created_date'] = date('Y-m-d');
        return $this->insert($data);
    }

    public function modify(array $data, $rowId)
    {
        return $this->update($data, $rowId);
    }

    public function getSanctionedPost($rowId)
    {
        $secondTable = 'departments';
        $thirdTable = 'categories';
        $fourthTable = 'designations';
        $fields = "{$this->table}.{$this->primaryKey},
                   {$secondTable}.name AS department,
                   {$thirdTable}.title AS category,
                   {$fourthTable}.title AS designation,
                   {$this->table}.no_of_sanctioned_posts";

        $this->db->select($fields);
        $this->db->from($this->table);
        $this->db->join("{$secondTable}",
               "{$secondTable}.department_id = {$this->table}.department_id");
        $this->db->join("{$fourthTable}",
               "{$fourthTable}.designation_id = {$this->table}.designation_id");
        $this->db->join("{$thirdTable}",
               "{$thirdTable}.category_id = {$fourthTable}.category_id");
        $this->db->where("{$this->table}.{$this->primaryKey}", $rowId);

        return $this->db->get()->row_array();
    }

    public function getSanctionedPostRow($rowId)
    {
        $otherTable = 'designations';
        $fields = "{$this->table}.{$this->primaryKey},
                   {$this->table}.department_id,
                   {$otherTable}.category_id,
                   {$this->table}.designation_id,
                   {$this->table}.no_of_sanctioned_posts";

        $this->db->select($fields);
        $this->db->from($this->table);
        $this->db->join("{$otherTable}",
               "{$otherTable}.designation_id = {$this->table}.designation_id");
        $this->db->where("{$this->table}.{$this->primaryKey}", $rowId);

        return $this->db->get()->row_array();
    }
    
    public function getReportByFacilityId($facilityId)
    {
       if (!empty($facilityId)) {
           
        $this->db->select("{$this->table}.facility_designation_id, {$this->table}.no_of_sanctioned_posts, 
            {$this->table}.no_of_filled_up_posts, D.name AS department, dg.title AS designation");
        $this->db->from($this->table);
        $this->db->join('departments AS D', "D.department_id= {$this->table}.department_id");
        $this->db->join('designations AS dg',"dg.designation_id = {$this->table}.designation_id");        
        $this->db->where("{$this->table}.facility_id", $facilityId);    
        $this->db->group_by("{$this->table}.department_id");
        
        return $this->db->get()->result_array();
       }
       return false;
    }

    public function incrementFilledUpPostNumber(array $data)
    {
        $sql = "UPDATE `{$this->table}`
                SET `no_of_filled_up_posts` = `no_of_filled_up_posts` + 1
                WHERE `facility_id` = '{$data['facility_id']}'
                AND `department_id` = '{$data['department_id']}'
                AND `designation_id` = '{$data['designation_id']}'";

        return $this->db->query($sql);
    }

    public function decrementFilledUpPostNumber(array $data)
    {
        $sql = "UPDATE `{$this->table}`
                SET `no_of_filled_up_posts` = `no_of_filled_up_posts` - 1
                WHERE `facility_id` = '{$data['facility_id']}'
                AND `department_id` = '{$data['department_id']}'
                AND `designation_id` = '{$data['designation_id']}'";

        return $this->db->query($sql);
    }

    public function checkVacancy(array $data)
    {
        $result = $this->find($data, 'no_of_sanctioned_posts, no_of_filled_up_posts');
        if (empty($result)) {
            return false;
        }

        return ($result['no_of_sanctioned_posts'] > $result['no_of_filled_up_posts']);
    }
}