<?php

/**
 * Facility Model
 *
 * This model helps in retrieving facility information based on
 * different criteria.
 *
 * @author      Md. Eftakhairul Islam <eftakhairul@rightbrainsolution.com>
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 * @package     DGHEALTH
 */
class Facility extends MY_Model
{
    public function __construct ()
    {
        parent::__construct();
        $this->loadTable('facilities', 'facility_id');
    }

    public function getAllFacilities($offset = null)
    {
        if (is_null($offset)) {
            return $this->findAll(null, "{$this->primaryKey}, name, upazilla_id", 'name ASC');
        } else {
            $limit = $this->config->item('rowsPerPage');
            return $this->findAll(null, "{$this->primaryKey}, name, upazilla_id", 'name ASC', $offset, $limit);
        }        
    }
    
    public function countAllFacility() 
    {
        return $this->db->count_all("{$this->table}");
    }

    public function getAllTypes()
    {
        return $this->db->orderby('name ASC')->get('types')->result_array();
    }

    public function getDetail($facilityId)
    {
        return $this->find(array($this->primaryKey => $facilityId));
    }

    public function getFullDetail($facilityId)
    {
        $fields = "{$this->table}.*, `div`.`name` AS `division`,
                   `dis`.`name` AS `district`, `upa`.`name` AS `upazilla`,
                   `types`.`name` AS `type`";
        $this->db->select($fields);
        $this->db->from($this->table);
        $this->db->join('divisions AS `div`',"div.division_id = {$this->table}.division_id");
        $this->db->join('districts AS `dis`',"dis.district_id = {$this->table}.district_id");
        $this->db->join('upazillas AS `upa`',"upa.upazilla_id = {$this->table}.upazilla_id");
        $this->db->join('types',"types.type_id = {$this->table}.type_id");
        $this->db->where("{$this->table}.{$this->primaryKey}", $facilityId);

        return $this->db->get()->row_array();
    }

    public function save(array $data)
    {
        $data['created_date'] = date('Y-m-d');
        return $this->insert($data);
    }

    public function modify(array $data, $facilityId)
    {
        return $this->update($data, $facilityId);
    }

    public function incrementDoctorNumber($facilityId)
    {
        $sql = "UPDATE `{$this->table}`
                SET `no_of_doctors` = `no_of_doctors` + 1
                WHERE `{$this->primaryKey}` = '{$facilityId}'";
        
        return $this->db->query($sql);
    }

    public function decrementStaffNumber($facilityId)
    {
        $sql = "UPDATE `{$this->table}`
                SET `no_of_doctors` = `no_of_doctors` - 1
                WHERE `{$this->primaryKey}` = '{$facilityId}'";

        return $this->db->query($sql);
    }

    public function getName ($facilityId)
    {
        $result = $this->find(array($this->primaryKey => $facilityId));
        return empty($result) ? false : $result['name'];
    }

    public function getNameId ($facilityId)
    {
        return $this->find(array($this->primaryKey => $facilityId));
    }

    public function getNoOfSanctionPost ($facilityId)
    {
        return $this->find(array($this->primaryKey => $facilityId), 'name, no_of_sanction_posts');
    }

    public function updateSanctionPost($data, $facilityId)
    {
        return $this->update($data, $facilityId);
    }
}