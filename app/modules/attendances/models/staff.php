<?php

/**
 * Staff Model
 *
 * @package     Model
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 * @author      Eftakhairul Islam <eftakhairul@gmail.com>
 */
class Staff extends MY_Model
{
    public function __construct ()
    {
        parent::__construct();
        $this->loadTable('staffs', 'staff_id');
    }

    public function getDoctorsList($facilityId)
    {
        $fields = "{$this->table}.*, designations.title AS designation,
                    departments.name AS department_name";
        $this->db->select($fields);
        $this->db->from($this->table);
        $this->db->join('departments', "{$this->table}.department_id=departments.department_id");
        $this->db->join('designations', "{$this->table}.designation_id=designations.designation_id");
        $this->db->where("{$this->table}.facility_id", $facilityId);
        $this->db->where("{$this->table}.status", 0);

        return $this->db->get()->result_array();
    }

    public function getAll ($facilityId)
    {
        $fields = "{$this->table}.{$this->primaryKey}, {$this->table}.name,
                   designations.title AS designation,
                   departments.name AS department_name,
                   categories.title AS category";

        $this->db->select($fields);
        $this->db->from($this->table);
        $this->db->join('departments', "{$this->table}.department_id=departments.department_id");
        $this->db->join('categories', "{$this->table}.category_id=categories.category_id");
        $this->db->join('designations', "{$this->table}.designation_id=designations.designation_id");
        $this->db->where("{$this->table}.facility_id", $facilityId);
        $this->db->where("{$this->table}.status", 0);

        return $this->db->get()->result_array();
    }

    public function getAllUnavailableStaffs ($facilityId)
    {
        $fields = "{$this->table}.{$this->primaryKey}, {$this->table}.name,
                   designations.title AS designation,
                   departments.name AS department_name,
                   categories.title AS category, {$this->table}.status";

        $this->db->select($fields);
        $this->db->from($this->table);
        $this->db->join('departments', "{$this->table}.department_id=departments.department_id");
        $this->db->join('categories', "{$this->table}.category_id=categories.category_id");
        $this->db->join('designations', "{$this->table}.designation_id=designations.designation_id");
        $this->db->where("{$this->table}.facility_id", $facilityId);
        $this->db->where("{$this->table}.status !=", 0);

        return $this->db->get()->result_array();
    }

    public function setAsRemoved($data, $staffId)
    {
        $this->db->where("{$this->primaryKey}", $staffId);        
        $this->db->update("{$this->table}", $data);     
    }
    
    public function save($data)
    {
        $CI = &get_instance();
        $CI->load->helper('date');
        $data['joining_date'] = human_to_mysql($data['joining_date']);
        $data['joining_date_govt'] = human_to_mysql($data['joining_date_govt']);
        $data['joining_date_position'] = human_to_mysql($data['joining_date_position']);
        $data['birth_date'] = human_to_mysql($data['birth_date']);
        $data['created_date'] = date('Y-m-d');
        return $this->insert($data);
    }

    public function update($data, $staffId)
    {
        $CI = &get_instance();
        $CI->load->helper('date');

        $data['joining_date'] = human_to_mysql($data['joining_date']);
        $data['birth_date'] = human_to_mysql($data['birth_date']);

        return parent::update($data, $staffId);
    }
    
    public function getDoctorNameById($staffId)
    {
        $this->db->select("{$this->table}.name, dept.name AS department, dg.title");
        $this->db->from($this->table);
        $this->db->join('designations AS dg',"dg.designation_id = {$this->table}.designation_id");
        $this->db->join('departments AS dept',"dept.department_id = {$this->table}.department_id"); 
        $this->db->where("{$this->table}.{$this->primaryKey}", $staffId);
        
        return $this->db->get()->row_array();                
    }
    
    public function getStaffDetailsById($staffId)
    {
        $this->db->select("{$this->table}.*");
        $this->db->from($this->table);
        $this->db->where("{$this->table}.{$this->primaryKey}", $staffId);
        
        return $this->db->get()->row_array(); 
    }

    public function getStaffProfile($staffId)
    {
        $fields = "{$this->table}.*, `dept`.`name` AS `department`,
                   `ct`.`title` AS `category`, `dg`.`title` AS `designation`";
        $this->db->select($fields);
        $this->db->from($this->table);
        $this->db->join('departments AS dept',"dept.department_id = {$this->table}.department_id");
        $this->db->join('categories AS ct',"ct.category_id = {$this->table}.category_id");
        $this->db->join('designations AS dg',"dg.designation_id = {$this->table}.designation_id");
        $this->db->where("{$this->table}.{$this->primaryKey}", $staffId);

        return $this->db->get()->row_array();
    }

    public function getPostingDetail($staffId)
    {
        return $this->find(array($this->primaryKey => $staffId), 'facility_id, department_id, designation_id');
    }
}