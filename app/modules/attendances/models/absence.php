<?php

/**
 * Absence
 *
 * @author      Eftakhairul Islam <eftakhairul@gmail.com>
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class Absence extends MY_Model
{
    public function __construct ()
    {
        parent::__construct();
        $this->loadTable('absences', 'absence_id');
    }

    public function save(array $data)
    {
        $data['created_date'] = date('Y-m-d');
        return $this->insert($data);
    }
    
    public function getRecordById($doctorId) 
    {
        $data = array("staff_id" => $doctorId);
        return $this->findAll($data, "created_date, reason, is_authorized");
    }
    
    public function countRows($data) 
    {   
        return count($this->getQueryResult($data));
    }
    
    public function searchById($data, $offset = 0) 
    {
        $count = $this->config->item('rowsPerPage');
        return $this->getQueryResult($data, $count, $offset);
    }

    private function getQueryResult($data, $count = null, $offset = 0)
    {        
        $selectQuery = "SELECT {$this->table}.created_date FROM `{$this->table}` WHERE `facility_id` = '{$data['facilityId']}'";
        if (!empty($data['startingDate'])) {
            $selectQuery .= " AND created_date BETWEEN '{$data['startingDate']}' AND '{$data['endingDate']}'";
        }

        $selectQuery .= " ORDER BY created_date DESC";
        
        if (!empty($count)) {
            $offset = (int) $offset;
            $selectQuery .= " LIMIT {$offset}, {$count}";
        }

        $resultDates = $this->db->query($selectQuery)->result_array();
        
        foreach ($resultDates AS $key => $row) {
            $resultDates[$key]["doctors"] = $this->_getDoctorsByDate($row['created_date']);
            
        }             
        
        return $resultDates;
    }
    
    private function _getDoctorsByDate($date)
    {
        $this->db->select("{$this->table}.staff_id, S.name, dept.name AS department, dg.title");
        $this->db->from($this->table);
        $this->db->join('staffs AS S', "S.staff_id= {$this->table}.staff_id");
        $this->db->join('designations AS dg','dg.designation_id = S.designation_id');
        $this->db->join('departments AS dept','dept.department_id = S.department_id');
        $this->db->where("{$this->table}.created_date", $date);     
        
        return $this->db->get()->result_array();

    }
}