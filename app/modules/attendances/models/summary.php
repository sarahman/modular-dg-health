<?php

/**
 * Summary Model
 *
 * This model helps in retrieving facility names those do not submit data currently
 *
 * @author      Md. Eftakhairul Islam <eftakhairul@rightbrainsolution.com>
 * @author      Syed Abidur Rahman <abid@rightbrainsolution.com>
 * @package     DGHEALTH
 */
class Summary extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->loadTable('facilities', 'facility_id');
    }

    public function findEmptyFacilities($data = array(), $offset = 0)
    {
        $selectClause = "SELECT `name`, `email_address`";
        $count = $this->config->item('rowsPerPage');
        return $this->_getQueryResultForNotReporting($data, $selectClause, $count, $offset);
    }

    public function countRows($data = array())
    {
        $selectClause = "SELECT COUNT(`name`) `count`";
        $result = $this->_getQueryResultForNotReporting($data, $selectClause);
        return $result[0]['count'];
    }

    private function _getQueryResultForNotReporting($data, $selectClause, $count = null, $offset = 0)
    {
        $data = $this->_refactorDates($data);
        $query = "{$selectClause}
                  FROM `{$this->table}`
                  WHERE `{$this->primaryKey}` NOT IN
                      (SELECT DISTINCT `{$this->primaryKey}` FROM `attendances`
                       WHERE `created_date` BETWEEN '{$data['startingDate']}' AND '{$data['endingDate']}')";

        if (!empty($count)) {
            $offset = (int)$offset;
            $query .= " LIMIT {$offset}, {$count}";
        }

        return $this->db->query($query)->result_array();
    }

    public function sumViewPost($data = array())
    {
        $where = empty($data) ? '' : "WHERE `type_id` = '{$data['type_id']}'";

        $query = "SELECT `{$this->primaryKey}`, `no_of_sanction_posts`, `no_of_doctors`
                  FROM `{$this->table}` {$where} GROUP BY `{$this->primaryKey}`";

        $results = $this->db->query($query)->result_array();
        $sanctionPostCount = 0;
        $filledPostCount = 0;

        foreach ($results as $result) {
            $sanctionPostCount = $sanctionPostCount + $result['no_of_sanction_posts'];
            $filledPostCount = $filledPostCount + $result['no_of_doctors'];
        }

        if ($filledPostCount === 0) {
            return array("no_of_sanction_posts" => $sanctionPostCount, "no_of_doctors" => 0, "parcentage" => 0);
        }

        return array("no_of_sanction_posts" => $sanctionPostCount, "no_of_doctors" => $filledPostCount);
    }

    public function absentStatus($data = array(), $offset = 0)
    {
        $select = "SELECT `F`.`{$this->primaryKey}`, `F`.`name`,
                   SUM(`F`.`no_of_sanction_posts`) AS `total_no_of_sanction_posts`,
                   SUM(`F`.`no_of_doctors`) AS `total_no_of_doctors`,
                   SUM(`A`.`no_of_authorized_absence`) AS `total_no_of_authorized_absence`,
                   AVG(`A`.`no_of_authorized_absence`) AS `avg_no_of_authorized_absence`,
                   SUM(`A`.`no_of_unauthorized_absence`) AS `total_no_of_unauthorized_absence`,
                   AVG(`A`.`no_of_unauthorized_absence`) AS `avg_no_of_unauthorized_absence`";
        $count = $this->config->item('rowsPerPage');
        return $this->_getQueryResult($data, $select, $count, $offset);
    }

    public function countRowsOfAbsence($data = array())
    {
        $select = "SELECT COUNT(`F`.`{$this->primaryKey}`) AS `count`";
        $result = $this->_getQueryResult($data, $select);
        return count($result);
    }

    private function _getQueryResult($data, $selectClause, $count = null, $offset = 0)
    {
        $dateRange = array();
        $data = $this->_refactorDates($data);

        $dateRange[] = "`A`.`created_date` BETWEEN '{$data['startingDate']}' AND '{$data['endingDate']}'";

        unset($data['startingDate']);
        unset($data['endingDate']);

        $CI = &get_instance();
        $CI->load->helper('database');
        $whereClause = getWhere($data, $dateRange);

        $sql = "{$selectClause}
                FROM `{$this->table}` AS `F`
                JOIN `attendances` AS `A`
                {$whereClause}
                GROUP BY `F`.`{$this->primaryKey}`
                ORDER BY `A`.`created_date` DESC";

        if (!empty($count)) {
            $offset = (int)$offset;
            $sql .= " LIMIT {$offset}, {$count}";
        }

        return $this->db->query($sql)->result_array();
    }

    public function sumOfAbsences($data = array())
    {
        $data = $this->_refactorDates($data);

        $selectAbsences = "SELECT `{$this->primaryKey}`, SUM(`no_of_authorized_absence`) AS `total_no_of_authorized_absence`,
                           SUM(`no_of_unauthorized_absence`) AS `total_no_of_unauthorized_absence`
                           FROM `attendances`
                           WHERE `created_date` BETWEEN '{$data['startingDate']}' AND '{$data['endingDate']}'
                           GROUP BY `{$this->primaryKey}`";

        if (!empty($data['type_id'])) {
            $selectAbsences = "SELECT DEV . *
                      FROM ({$selectAbsences}) AS `DEV`
                      INNER JOIN `{$this->table}` AS `F` ON `F`.`{$this->primaryKey}` = `DEV`.`{$this->primaryKey}`
                      WHERE `F`.`type_id` = '{$data['type_id']}'";
        }

        $selectAbsences = "SELECT SUM(`A`.`total_no_of_authorized_absence`) AS `total_no_of_authorized_absence`,
                                SUM(`A`.`total_no_of_unauthorized_absence`) AS `total_no_of_unauthorized_absence`
                           FROM ({$selectAbsences}) AS `A`";

        $where = empty($data['type_id']) ? '' : "WHERE `F1`.`type_id` = '{$data['type_id']}'";
        $selectPosts = "Select `F1`.`{$this->primaryKey}`, `F1`.`no_of_sanction_posts`, `F1`.`no_of_doctors`
                        FROM `{$this->table}` AS `F1` {$where}";

        $selectPosts = "SELECT SUM(`F`.`no_of_sanction_posts`) AS `total_no_of_sanction_posts`,
                            SUM(`F`.`no_of_doctors`) AS `total_no_of_doctors`
                        FROM ({$selectPosts}) AS `F`";

        $resultOfAbsences = $this->db->query($selectAbsences)->first_row('array');
        $resultOfPosts = $this->db->query($selectPosts)->first_row('array');
        $result = array_merge($resultOfAbsences, $resultOfPosts);

        return array(
            'total_no_of_sanction_posts' => $result['total_no_of_sanction_posts'] ? $result['total_no_of_sanction_posts'] : 0,
            'total_no_of_doctors' => $result['total_no_of_doctors'] ? $result['total_no_of_doctors'] : 0,
            'total_no_of_authorized_absence' => $result['total_no_of_authorized_absence'] ? $result['total_no_of_authorized_absence'] : 0,
            'total_no_of_unauthorized_absence' => $result['total_no_of_unauthorized_absence'] ? $result['total_no_of_unauthorized_absence'] : 0
        );
    }

    private function _refactorDates($data = array())
    {
        $CI = &get_instance();
        $CI->load->helper('date');

        if (!empty($data['startingDate'])) {
            $data['startingDate'] = human_to_mysql($data['startingDate']);
            if (!empty($data['endingDate'])) {
                $data['endingDate'] = human_to_mysql($data['endingDate']);
            }
        }

        if (empty($data['startingDate'])) {
            $data['startingDate'] = php_to_mysql();
        }

        if (empty($data['endingDate'])) {
            $data['endingDate'] = php_to_mysql();
        }

        return $data;
    }
}