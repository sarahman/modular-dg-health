<?php

/**
 * Designation Model
 *
 * @package     Model
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class Designation extends MY_Model
{
    public function __construct ()
    {
        parent::__construct();
        $this->loadTable('designations', 'designation_id');
    }

    public function getAll()
    {
        return $this->findAll(null, '*', 'title ASC');
    }

    public function getAllByCategory($categoryId)
    {
        return $this->findAll(array('category_id' => $categoryId), '*', 'title ASC');
    }
}