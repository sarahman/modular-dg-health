<?php

/**
 * Authorize Absences Model
 *
 * @author      Eftakhairul Islam <eftakhairul@gmail.com>
 */
class Authorized_absence extends MY_Model
{
    public function __construct ()
    {
        parent::__construct();
        $this->loadTable('authorize_absences', 'authorize_absence_id');
    }

    public function save(array $data)
    {
        $CI = &get_instance();
        $CI->load->helper('date');
        $data['date_from'] = date('Y-m-d');
        $data['date_to'] = human_to_mysql($data['date_to']);
        return $this->insert($data);
    }

    public function getAuthorizedStaffsByFacilityId($facilityId)
    {
        $date = date('Y-m-d');

        $this->db->select("staff_id");
        $this->db->from($this->table);
        $this->db->where('facility_id', $facilityId);
        $this->db->where("date_from <= '{$date}'");
        $this->db->where("date_to >= '{$date}'");

        $result =  $this->db->get()->result_array();
        $data = array();

        foreach($result AS $row) {
            $data[] = $row['staff_id'];
        }

        return $data;
    }
}