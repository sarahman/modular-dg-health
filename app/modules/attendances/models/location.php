<?php

/**
 * Locations Model
 *
 * @author      Eftakhairul
 */
class Location extends MY_Model
{
    public function getAllDivisions()
    {
        return $this->db->query("SELECT * FROM divisions")->result_array();
    }

    public function getAllDistricts()
    {
        return $this->db->query("SELECT * FROM districts")->result_array();
    }

    public function getAllUpazillas()
    {
        return $this->db->query("SELECT * FROM upazillas")->result_array();
    }
}