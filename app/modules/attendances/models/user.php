<?php

/**
 * Description of Users
 *
 * @author      eftakhairul
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class User extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->loadTable('users', 'user_id');
    }

    public function validateUser($data)
    {
        $data['password'] = md5($data['password']);
        return $this->find($data, 'username, type, facility_id');
    }

    public function save(array $data)
    {
        $data['password'] = md5($data['password']);
        return $this->insert($data);
    }
    
    public function changePassword($data, $facilityId)
    {
        if (!empty($data['password'])) {
            $data['password'] = md5($data['password']);
        } else if (!empty($data['new_password'])) {
            $data['password'] = md5($data['new_password']);
        }
        
        $select = "UPDATE `{$this->table}`
                   SET `password` = '{$data['password']}' 
                   WHERE `facility_id` = '{$facilityId}'";
        
        $this->db->query($select );        
    }

    public function validatePassword(array $data)
    {
        $data['password'] = md5($data['password']);
        $result = $this->find($data);

        return !empty($result);
    }
}