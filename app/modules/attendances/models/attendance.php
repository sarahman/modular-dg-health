<?php

/**
 * Attendance
 * 
 * @author      Eftakhairul Islam <eftakhairul@gmail.com>
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class Attendance extends MY_Model
{
    public function __construct ()
    {
        parent::__construct();
        $this->loadTable('attendances', 'attendance_id');
    }

    public function save(array $data)
    {
        $data['created_date'] = date('Y-m-d');
        return $this->insert($data);
    }

    public function searchForGraph($data = array(), $offset = 0)
    {
        $select = "SELECT `A`.`facility_id`, `A`.`no_of_authorized_absence`,
                    `A`.`no_of_unauthorized_absence`";
        $count = $this->config->item('rowsPerPage');
        return $this->_getQueryResult($data, $select, $count, $offset);
    }

    public function search($data = array(), $offset = 0)
    {
        $select = "SELECT `F`.`facility_id`, `F`.`name`, `F`.`no_of_sanction_posts`,
                   `F`.`no_of_doctors`, `A`.`no_of_authorized_absence`,
                   `A`.`no_of_unauthorized_absence`, `A`.`created_date`";
        $count = $this->config->item('rowsPerPage');
        return $this->_getQueryResult($data, $select, $count, $offset);
    }

    public function countRows ($data = array())
    {
        $select = "SELECT COUNT(*) AS `count`";
        $result = $this->_getQueryResult($data, $select);
        return $result[0]['count'];
    }

    private function _getQueryResult ($data, $selectClause, $count = null, $offset = 0)
    {
        $CI = &get_instance();
        $CI->load->helper('date');
        $CI->load->helper('database');
        $dateRange = array();

        if (!empty($data['startingDate']) || !empty($data['date']) || !empty($data['year'])) {

            if (!empty($data['startingDate'])) {
                $startDate = human_to_mysql($data['startingDate']);
                if (!empty($data['endingDate'])) {
                    $endDate = human_to_mysql($data['endingDate']);
                }
            } else if (!empty($data['date'])) {
                $startDate = human_to_mysql($data['date']);
                $endDate = human_to_mysql($data['date']);
                unset($data['date']);
            } else if (!empty($data['year'])) {
                if (!empty($data['month'])) {
                    $startDate = $data['year'] . '-' . $data['month'] . '-01';
                    $endDate = $data['year'] . '-' . $data['month'] . '-31';
                    unset($data['month']);
                } else {
                    $startDate = $data['year'] . '-01-01';
                    $endDate = $data['year'] . '-12-31';
                }
                unset($data['year']);
            }

            if (empty($startDate)) {
                $startDate = php_to_mysql();
            }

            if (empty($endDate)) {
                $endDate = php_to_mysql();
            }

            $dateRange[] = "`A`.`created_date` BETWEEN '{$startDate}' AND '{$endDate}'";

            unset($data['startingDate']);
            unset($data['endingDate']);
        }

        $whereClause = getWhere($data, $dateRange);

        $sql = "{$selectClause}
                FROM `{$this->table}` AS `A`
                INNER JOIN `facilities` AS `F` USING(`facility_id`)
                {$whereClause} ORDER BY `A`.`created_date` DESC";

        if (!empty($count)) {
            $offset = (int) $offset;
            $sql .= " LIMIT {$offset}, {$count}";
        }

        return $this->db->query($sql)->result_array();
    }

    public function getFirstYear()
    {
        $selectQuery = "SELECT YEAR(`created_date`) AS `year`
                        FROM `{$this->table}`
                        ORDER BY `created_date` ASC";

        $result = $this->db->query($selectQuery)->first_row('array');
        return empty($result) ? date('Y') : $result['year'];
    }
    
    public function getAbsencesByCurrentDate($facilityId)
    {
        $this->db->select("{$this->table}.*, F.no_of_sanction_posts, F.no_of_doctors");
        $this->db->from($this->table);
        $this->db->join('facilities AS F', "F.facility_id = {$this->table}.facility_id");
        $this->db->where("{$this->table}.facility_id", $facilityId);     
        $this->db->order_by("{$this->table}.created_date", "desc");         
        $this->db->limit(1);
        
        return $this->db->get()->row_array();
    }
    
}