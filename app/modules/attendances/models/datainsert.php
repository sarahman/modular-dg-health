<?php

class Datainsert extends MY_Model
{
    public function insertData()
    {
        $this->_insertAdminUsers();
        $misDb_facilities = "SELECT * FROM `mis_db`.`facilities`";
        $data = $this->db->query($misDb_facilities)->result_array();

        foreach($data AS $row) {
            $this->_insertIntoFacilities($row);
        }

        echo 'Alhamdulillah, we are done!!!';
    }

    private function _insertAdminUsers()
    {
        $query = "INSERT INTO `users` (`user_id`, `username`, `password`, `type`, `facility_id`) VALUES
                    (1, 'dohs', '96e79218965eb72c92a549dd5a330112', 'user', 1),
                    (2, 'admin', '9dda8d0571dd07e325a2a680e65eee72', 'admin', NULL),
                    (3, 'abid', '96e79218965eb72c92a549dd5a330112', 'admin', NULL),
                    (4, 'myadmin', '543e82c4c230d8f8dff802530b6a26ef', 'admin', NULL)";

        $this->db->query($query);
    }

    private function _insertIntoFacilities($row)
    {
        $queryForFacilities = "INSERT INTO `facilities`
            (`name`, `division_id`, `district_id`, `upazilla_id`, `type_id`) VALUES
            ({$this->db->escape($row['name'])}, '{$row['division_id']}', '{$row['district_id']}',
             '{$row['upazilla_id']}', '{$row['type_id']}')";


        $this->db->query($queryForFacilities);
        $facilityId = $this->db->insert_id();
        $this->_insertIntoUsers($row, $facilityId);
    }

    private function _insertIntoUsers($row, $facilityId)
    {
        $misDb_mcTb = "SELECT * FROM `mis_db`.`mc_tb`
                WHERE `inst_id`={$this->db->escape($row['username'])}";

        $data = $this->db->query($misDb_mcTb)->row_array();
        if (empty($data)) {
            return;
        }

        $queryForUsers = "INSERT INTO `users`
            (`username`, `password`, `type`, `facility_id`) VALUES
            ({$this->db->escape($data['inst_id'])}, MD5('{$data['password']}'), 'user', '{$facilityId}')";

        $this->db->query($queryForUsers);
    }

    public function setDepartment($data)
    {
        $query = "INSERT INTO `designations`(`title`) VALUES ({$this->db->escape($data)})";
        
        $this->db->query($query);
        
    }
}