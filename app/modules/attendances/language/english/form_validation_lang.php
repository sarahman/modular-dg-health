<?php
$lang['required'] = "Please enter <strong>%s</strong>.<br />";
$lang['valid_email'] = "Please enter <strong>%s</strong> correctly.<br />";
$lang['alpha_dash'] = "Please enter <strong>%s</strong> correctly.<br />";
$lang['max_length'] = "Please enter <strong>%s at most 32 characters</strong>.<br />";
$lang['min_length'] = "Please enter <strong>%s at least 6 characters</strong>.<br />";
$lang['numeric'] = "Please enter <strong>numbers</strong> only.<br />";
$lang['matches'] = "Password does not match.";
$lang['is_username_available'] = "The <strong>%s</strong> is not available.";
$lang['website_available'] = "The <strong>%s</strong> is not available.";