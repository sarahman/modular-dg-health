<?php

/**
 * Description of Refactor Helper
 *
 * @author      Eftakhairul
 * @author      Syed Abidur Rahman
 */
if (!function_exists('eliminateNullField')) {
    function eliminateNullField($data)
    {
        foreach($data AS $key => $value) {

            if (!$value) {
                unset($data["{$key}"]);
            }
        }

        return $data;
    }
}