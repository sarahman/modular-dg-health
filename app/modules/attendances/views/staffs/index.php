<div class="block">

    <div class="block_head">

        <h2>All Staffs in <?php echo $facilityName ?></h2>

        <ul>
            <li>
                <a href="<?php echo site_url('attendances/facilities')?>">Back to Home Page</a>
            </li>
        </ul>

    </div>
    <!--.block_head ends -->

    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">

            <thead>

            <tr>
                <th>Staff Name</th>
                <th>Department</th>
                <th>Category</th>
                <th>Designation</th>
                <th>Action</th>
            </tr>

            </thead>

            <tbody>
            <?php if (empty($staffs)) : ?>

            <tr>
                <td colspan="5" class="nodatamsg">No data is found.</td>
            </tr>

            <?php else : foreach ($staffs as $facility) : ?>

            <tr>
                <td><?php echo $facility['name'] ?></td>
                <td><?php echo $facility['department_name'] ?></td>
                <td><?php echo $facility['category'] ?></td>
                <td><?php echo $facility['designation'] ?></td>
                <td>
                    <a href="<?php echo site_url("attendances/staffs/view/{$facility['staff_id']}") ?>">View</a> |
                    <a href="<?php echo site_url("attendances/staffs/edit/{$facility['staff_id']}") ?>">Edit</a>
                </td>
            </tr>

            <?php endforeach; endif; ?>

            </tbody>

        </table>

        <div class="pagination right">
            <?php //echo $this->pagination->create_links() ?>
        </div>
        <!--.pagination ends-->

    </div>
    <!--.block_content ends-->

</div> <!--.block ends-->