<div class="block">

    <?php if (!empty($errorMessage)) : ?>
    <div class="message errormsg">
        <?php echo $errorMessage ?>
    </div>
    <?php endif ?>

    <div class="block_head">
        <h2>Edit Staff | Organization: <?php echo $facilityName; ?></h2>
    </div>

    <div class="block_content">
        
        <form action="" method="POST">

            <p>
                <label for="name">
                    Staff Name: <span class="required">*</span>
                </label>

                <input id="name" type="text" name="name" class="text small"
                       value= "<?php echo set_value('name', $staff['name'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('name') ?>
                </span>
            </p>

            <p>
                <label for="department_id">
                    Department: <span class="required">*</span>
                </label>

                <select id="department_id" name="department_id" class="styled">
                    <option value=''>- Select Department -</option>

                    <?php foreach ($departments as $department) : ?>
                        <option value="<?php echo $department['department_id'] ?>"
                            <?php echo (($staff['department_id'] == $department['department_id'])? "selected='selected'" : '') ?>>
                            <?php echo $department['name'] ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('department_id') ?>
                </span>
            </p>
            
            <p>
                <label for="category_id">
                    Category: <span class="required">*</span>
                </label>

                <select id="category_id" name="category_id" class="styled">
                    <option value=''>- Select Staff Category -</option>

                    <?php foreach ($categories as $category) : ?>
                        <option value="<?php echo $category['category_id'] ?>"
                            <?php echo (($staff['category_id'] == $category['category_id'])? "selected='selected'" : '') ?> >
                            <?php echo $category['title'] ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('category_id') ?>
                </span>
            </p>

            <p>
                <label for="designation_id">
                    Designation: <span class="required">*</span>
                </label>

                <select id="designation_id" name="designation_id" class="styled">
                    <option value='<?php echo $this->input->post('designation_id') ? $this->input->post('designation_id') : $staff['designation_id'] ?>'
                        >- Select Designation -</option>
                </select>
                <span class='note error'>
                    <?php echo form_error('designation_id') ?>
                </span>
            </p>

            <p>
                <label for="designation_status">
                    Designation Status: <span class="required">*</span>
                </label>

                <select id="designation_status" name="designation_status" class="styled">
                    <option value=''>- Select Designation Status -</option>

                    <?php foreach ($this->config->item('designationStatuses') as $key => $value) : ?>
                        <option value="<?php echo $key ?>"
                            <?php echo (($staff['designation_status'] == $key)? "selected='selected'":"") ?>>
                            <?php echo $value ?></option>
                    <?php endforeach ?>
                </select>
                <span class='note error'>
                    <?php echo form_error('designation_status') ?>
                </span>
            </p>
            
            <p>
                <label for="pds_id_no">
                    Online PDS ID No.: <span class="required">*</span>
                </label>

                <input id="pds_id_no" type="text" name="pds_id_no" class="text small"
                       value= "<?php echo set_value('pds_id_no', $staff['pds_id_no'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('pds_id_no') ?>
                </span>
            </p>
            
            <p>
                <label for="dghs_code_no">
                    DGHS Code No. : <span class="required">*</span>
                </label>

                <input id="dghs_code_no" type="text" name="dghs_code_no" class="text small"
                       value= "<?php echo set_value('dghs_code_no',  $staff['dghs_code_no'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('dghs_code_no') ?>
                </span>
            </p>
            
            <p>
                <label for="job_class">
                    Job Class: <span class="required">*</span>
                </label>

                <select id="job_class" name="job_class" class="styled">
                    <option value=''>- Select Job Class -</option>

                    <?php foreach ($this->config->item('job_class') as $key => $row) : ?>
                        <option value="<?php echo $key ?>"
                            <?php echo (($staff['job_class'] == $key )? "selected='selected'":"") ?>>
                            <?php echo $row ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('job_class') ?>
                </span>
            </p>
            
            <p>
                <label for="father_name">
                    Father Name: <span class="required">*</span>
                </label>

                <input id="father_name" type="text" name="father_name" class="text small"
                       value= "<?php echo set_value('father_name', $staff['father_name'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('father_name') ?>
                </span>
            </p>
            
            <p>
                <label for="mother_name">
                    Mother Name: <span class="required">*</span>
                </label>

                <input id="mother_name" type="text" name="mother_name" class="text small"
                       value= "<?php echo set_value('mother_name', $staff['mother_name']) ?>" /><br />
                <span class='note error'>
                    <?php echo form_error('mother_name') ?>
                </span>
            </p>
            
            <p>
                <label>
                    Sex: <span class="required">*</span>
                </label>
                
                <input type="radio" name="sex" value="1" class ="radio" <?php echo ($staff['sex'] == 1)? 'checked="checked"' : ''; ?>> Male &nbsp;
                <input type="radio" name="sex" value="2" class ="radio" <?php echo ($staff['sex'] == 2) ? 'checked="checked"' : ''; ?>>Female
               
                <span class='note error'>
                    <?php echo form_error('sex') ?>
                </span>
            </p>
            
             <p>
                <label for="marital_status">
                   Marital Status: <span class="required">*</span>
                </label>

                <select id="marital_status" name="marital_status" class="styled">
                    <option value=''>- Select Marital Status -</option>

                    <?php foreach ($this->config->item('marital_status') as $key => $row) : ?>
                        <option value="<?php echo $key ?>"
                            <?php echo (($staff['marital_status'] == $key)? "selected='selected'":"") ?>>
                            <?php echo $row ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('marital_status') ?>
                </span>
            </p>
            
             <p>
                <label for="religion">
                   Religion: <span class="required">*</span>
                </label>

                <select id="religion" name="religion" class="styled">
                    <option value=''>- Select Religion -</option>

                    <?php foreach ($this->config->item('religion') as $key => $row) : ?>
                        <option value="<?php echo $key ?>"
                            <?php echo (($staff['religion'] == $key)? "selected='selected'":"") ?>>
                            <?php echo $row ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('religion') ?>
                </span>
            </p>
            
             <p>
                <label for="birth_date">
                    Date of Birth: <span class="required">*</span>
                </label>

                <input id="birth_date" type="text" name="birth_date" class="text date_picker" value= "<?php echo set_value('birth_date', $staff['birth_date'] )?>" /><br />
                <span class='note error'>
                    <?php echo form_error('birth_date') ?>
                </span>
            </p>
            
            <p>
                <label for="contact_no">
                   Contact No: <span class="required">*</span>
                </label>

                <input id="contact_no" type="text" name="contact_no" class="text small" value= "<?php echo set_value('contact_no', $staff['contact_no'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('contact_no') ?>
                </span>
            </p>
            
            <p>
                <label for="email_address">
                  Email Address: <span class="required">*</span>
                </label>

                <input id="email_address" type="text" name="email_address" class="text small" value= "<?php echo set_value('email_address', $staff['email_address'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('email_address') ?>
                </span>
            </p>
            
             <p>
                <label for="joining_date">
                    Date of joining health place : <span class="required">*</span>
                </label>

                <input id="joining_date" type="text" name="joining_date" class="text date_picker" value= "<?php echo set_value('joining_date', $staff['joining_date'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('joining_date') ?>
                </span>
            </p>

             <p>
                <label for="joining_date_govt">
                    Date of joining to Govt. Organization : <span class="required">*</span>
                </label>

                <input id="joining_date_govt" type="text" name="joining_date_govt" class="text date_picker" value= "<?php echo set_value('joining_date_govt', $staff['joining_date_govt'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('joining_date_govt') ?>
                </span>
            </p>

            <p>
                <label for="joining_date_position">
                    Date of joining to this position : <span class="required">*</span>
                </label>

                <input id="joining_date_position" type="text" name="joining_date_position" class="text date_picker" value= "<?php echo set_value('joining_date_position', $staff['joining_date_position'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('joining_date_position') ?>
                </span>
            </p>

            <p>
                <input type="submit" value="Save" class="submit small" />
                <input type="hidden" name="staff_id" value="<?php echo $staff['staff_id'] ?>" />
                <input type="button" value="Exit" class="submit small" onClick = "window.location = '<?php echo site_url('attendances/staffs')?>'" />
            </p>

        </form>

    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->

<script type="text/javascript">

    $(function() {
        if ($('#category_id').val() != '') {
            getDesignations($('#category_id').val())
        }
        $('#category_id').live('change', function(){
            getDesignations($(this).val())
        });

        function getDesignations(category)
        {
            $.ajax({
                url: '<?php echo site_url('ajax/getDesignationsList') ?>',
                type: 'POST',
                data: {'category_id': category, 'designation_id': $('#designation_id').val()},
                success: function(options){
                    $('#designation_id').html(options);
                }
            });
        }
    });

</script>
