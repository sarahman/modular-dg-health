<div class="block">

    <div class="block_head">
        <h2>Staff Profile</h2>
    </div>

    <div class="block_content">
        <fieldset class="small">
            <legend>Personal Detail</legend>

            <span class="title">Name: </span>
            <span class="description"><?php echo $staff['name'] ?></span><br /><br />

            <span class="title">Father's Name: </span>
            <span style="font-size: medium"><?php echo $staff['father_name'] ?></span><br /><br />

            <span class="title">Mother's Name: </span>
            <span style="font-size: medium"><?php echo $staff['mother_name'] ?></span><br /><br />
            
            <span class="title">Date of Birth: </span>
            <span style="font-size: medium"><?php echo $staff['birth_date'] ?></span><br /><br />

            <span class="title">Religion: </span>
            <span style="font-size: medium"><?php echo $staff['religion'] ?></span><br /><br />
            
            <span class="title">Sex: </span>
            <span style="font-size: medium"><?php echo $staff['sex'] ?></span><br /><br />

            <span class="title">Marital Status: </span>
            <span style="font-size: medium"><?php echo $staff['marital_status']; ?></span><br /><br />
            
            <span class="title">Contact No: </span>
            <span style="font-size: medium"><?php echo $staff['contact_no'] ?></span><br /><br />

            <span class="title">Email Address: </span>
            <span style="font-size: medium"><?php echo $staff['email_address'] ?></span><br /><br />
        </fieldset>

        <fieldset class="small">
            <legend>Institutional Detail</legend>

            <span class="title">Department Name: </span>
            <span style="font-size: medium"><?php echo $staff['department'] ?></span><br /><br />

            <span class="title">Category: </span>
            <span style="font-size: medium"><?php echo $staff['category'] ?></span><br /><br />

            <span class="title">Designation: </span>
            <span style="font-size: medium"><?php echo $staff['designation'] ?></span><br /><br />

            <span class="title">Job Class: </span>
            <span style="font-size: medium"><?php echo $staff['job_class'] ?></span><br /><br />

            <span class="title">PDS ID No.: </span>
            <span style="font-size: medium"><?php echo $staff['pds_id_no'] ?></span><br /><br />

            <span class="title">DGHS Code No.: </span>
            <span style="font-size: medium"><?php echo $staff['dghs_code_no'] ?></span><br /><br />
        </fieldset>
        
        <div style="padding: 5px 15px 30px 20px; font-size: 14px;">
            <a href="javascript:history.go(-1)" style="padding: 0px 30px">Click here to go back</a>
        </div>

    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->