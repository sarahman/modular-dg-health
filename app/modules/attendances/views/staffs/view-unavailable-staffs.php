<div class="block">

    <div class="block_head">

        <h2>All Transferred Staffs in <?php echo $facilityName ?></h2>

        <ul>
            <li>
                <a href="<?php echo site_url('attendances/facilities')?>">Back to Home Page</a>
            </li>
        </ul>

    </div>
    <!--.block_head ends -->

    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">

            <thead>

            <tr>
                <th>Staff Name</th>
                <th>Department</th>
                <th>Category</th>
                <th>Designation</th>
                <th>Status</th>
            </tr>

            </thead>

            <tbody>

            <?php if (empty($unavailableStaffs)) : ?>

            <tr>
                <td colspan="4" class="nodatamsg">No data has not found.</td>
            </tr>

            <?php else : foreach ($unavailableStaffs as $staff) : ?>

            <tr>
                <td><?php echo $staff['name'] ?></td>
                <td><?php echo $staff['department_name'] ?></td>
                <td><?php echo $staff['category'] ?></td>
                <td><?php echo $staff['designation'] ?></td>
                <td><?php echo $events[$staff['status']] ?></td>
            </tr>

            <?php endforeach; endif; ?>

            </tbody>

        </table>

        <div class="pagination right">
            <?php //echo $this->pagination->create_links() ?>
        </div>
        <!--.pagination ends-->

        <div style="padding: 5px 15px 30px 20px; font-size: 14px;">
            <a href="javascript:history.go(-1)" style="padding: 0px 30px">Click here to go back</a>
        </div>

    </div>
    <!--.block_content ends-->

</div> <!--.block ends-->