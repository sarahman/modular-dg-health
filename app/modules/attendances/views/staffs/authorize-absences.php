<div class="block small" id="absent">

    <?php if (!empty($errorMessage)) : ?>
    <div class="message errormsg">
        <?php echo $errorMessage ?>
    </div>
    <?php endif ?>

    <div class="block_head">
        <h2>Enter Date</h2>
    </div>

    <div class="block_content">
        
       <form action="<?php echo site_url('attendances/staffs/authorizeAbsences') ?>"
              id="add-event-form" method="POST">
            
            <p>
                <label for="date">
                   Authorized Absence up to the Date: <span class="required">*</span>
                </label>

                <input id="date" type="text" name="date" class="text date_picker" value= "<?php echo set_value('date')?>" /><br />
                <span class='note error'>
                    <?php echo form_error('date') ?>
                </span>
            </p>
            
            <p>                 
                <input type="submit" value="Save" id="submit-event" class="submit small" />
                <input type="button" value="Cancel" id="cancel" class="submit small" />
            </p>
            
        </form>

    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->

<script type="text/javascript" src="<?php echo site_url('assets/js/jquery.date_input.pack.js') ?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/custom.js') ?>"></script>
<script language="javascript" src="<?php echo site_url('assets/js/facebox/facebox.js')?>"></script>
<script type="text/javascript">
    $(function(){
    
        $('#submit-event').live('click', function(){

            $.ajax({
                url: $("#add-event-form").attr('action'),
                type: 'POST',
                data: {
                    date: $('#date').val(),
                    staff_id: $('#staffId_'+value).val()
                    },
                success: function(content) {
                    if (content == 'true') {                       
                        
                        return closeFacebox();
                    } else {
                        $('#absent').replaceWith(content);
                    }
                }
            });

            return false;
        });

        $('#cancel').live('click', function(){
            return closeFacebox();
        });
    });

    function closeFacebox() {
        $.facebox.close();
        return false;
    }

</script>