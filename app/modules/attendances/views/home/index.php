<div class="block">

    <div class="block_head">
        <h2>Absence of Facilities</h2>

        <form method="POST" action="">

            <input type="text" name="date" class="text date_picker"
                   value="<?php echo empty($filters['date']) ? 'Enter Date' : mysql_to_human($filters['date']) ?>" />
            
            <select name="division_id" id="parent">

                <option value="">- Select Division -</option>

                <?php if ($divisions): foreach($divisions as $division) : ?>

                <option value="<?php echo $division['division_id'] ?>" <?php echo (!empty($filters['division_id']) && $filters['division_id'] == $division['division_id']) ? "selected = 'selected'" : '' ?>>
                    <?php echo $division['name'] ?>
                </option>

                <?php endforeach; endif ?>

            </select>

            <select id="child" name="district_id">

                <option value="">- Select District -</option>

                <?php if ($districts): foreach($districts as $district) : ?>

                <option class="sub_<?php echo $district['division_id'] ?>" value="<?php echo $district['district_id'] ?>" <?php echo (!empty($filters['district_id']) && $filters['district_id'] == $district['district_id']) ? "selected = 'selected'" : '' ?>>
                    <?php echo $district['name'] ?>
                </option>

                <?php endforeach; endif ?>

            </select>

            <select id="grandson" name="upazilla_id">

                <option value="">- Select Upazilla -</option>

                <?php if ($upazillas): foreach($upazillas as $upazilla) : ?>

                <option class="sub_<?php echo $upazilla['district_id'] ?>" value="<?php echo $upazilla['upazilla_id'] ?>" <?php echo (!empty($filters['upazilla_id']) && $filters['upazilla_id'] == $upazilla['upazilla_id']) ? "select = 'selected'" : '' ?>>
                    <?php echo $upazilla['name'] ?>
                </option>

                <?php endforeach; endif ?>

            </select>

            <select id="grandgrandson" name="facility_id">
                <option value="">- Select -</option>
                <?php if ($facilities) :
                foreach($facilities as $facility) : ?>

                <option class="sub_<?php echo $facility['upazilla_id'] ?>" value="<?php echo $facility['facility_id'] ?>" <?php echo (!empty($filters['facility_id']) && $filters['facility_id'] == $facility['facility_id']) ? "selected = 'selected'" : '' ?>>
                    <?php echo $facility['name'] ?>
                </option>

                <?php endforeach; endif ?>

            </select>
            
            <input type ="submit" value ="Submit" />

        </form>

    </div>		<!-- .block_head ends -->

    <div class="block_content tab_content" id="days">
        <?php if (empty($facilitiesFound)) : ?>

        <table align="center" style="margin: auto;" width="100%"><tr>
            <td class="nodatamsg" colspan="2" width="100%">Data has not found</td>
        </tr></table>

        <?php else : ?>

        <table class="stats" cellpadding="0" cellspacing="0" width="100%">

            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <?php foreach($facilitiesFound as $facilityFound) : ?>
                    <th><?php echo $facilityFound['facility_id'] ?></th>
                    <?php endforeach ?>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <th scope="row">Unauthorized Absence</th>
                    <?php foreach($facilitiesFound as $facilityFound) : ?>
                    <td><?php echo $facilityFound['no_of_unauthorized_absence'] ?></td>
                    <?php endforeach ?>
                </tr>

                <tr>
                    <th scope="row">Authorized Absence</th>
                    <?php foreach($facilitiesFound as $facilityFound) : ?>
                    <td><?php echo $facilityFound['no_of_authorized_absence'] ?></td>
                    <?php endforeach ?>
                </tr>
                
            </tbody>

        </table>

        <div class="pagination right">
            <?php echo $this->pagination->create_links() ?>
        </div> <!--.pagination ends-->
        
        <?php endif ?>

    </div><!-- .block_content ends -->

</div>		<!-- .block ends -->

<script type="text/javascript" src="<?php echo site_url('assets/js/search-facility.js')?>"></script>
<script type="text/javascript">
    var selectedDistrict = "<?php echo (!empty($filters['district_id'])) ? $filters['district_id'] : 0 ?>";
    var selectedUpazilla = "<?php echo (!empty($filters['upazilla_id'])) ? $filters['upazilla_id'] : 0 ?>";
    var selectedFacility = "<?php echo (!empty($filters['facility_id'])) ? $filters['facility_id'] : 0 ?>";
</script>