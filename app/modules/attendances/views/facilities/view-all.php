<div class="block">

    <div class="block_head">
        <h2>Organization Detail</h2>
    </div>

    <div class="block_content">

        <fieldset class="small">

            <legend>Organization Detail</legend>

            <span class="title">Local ID: </span>
            <span class="description"><?php echo $facility['local_id'] ?></span><br /><br />

            <span class="title">Name: </span>
            <span class="description"><?php echo $facility['name'] ?></span><br /><br />

            <span class="title">Division: </span>
            <span class="description"><?php echo $facility['division'] ?></span><br /><br />

            <span class="title">District: </span>
            <span class="description"><?php echo $facility['district'] ?></span><br /><br />
            
            <span class="title">Upazilla: </span>
            <span class="description"><?php echo $facility['upazilla'] ?></span><br /><br />

            <span class="title">Type: </span>
            <span class="description"><?php echo $facility['type'] ?></span><br /><br />

            <span class="title">Name of Chairperson: </span>
            <span class="description"><?php echo $facility['chairperson'] ?></span><br /><br />

            <span class="title">Contact No: </span>
            <span class="description"><?php echo $facility['contact_no'] ?></span><br /><br />
            
            <span class="title">Email Address: </span>
            <span class="description"><?php echo $facility['email_address'] ?></span><br /><br />

            <span class="title">Record Created: </span>
            <span class="description"><?php echo $facility['created_date'] ?></span><br /><br />

            <span class="title">Designations of the facility: </span><br /><br />
            <span>
                <table>
                    <thead>
                        <tr>
                            <th class="centered">Designation</th>
                            <th class="centered">No. of Sanctioned Posts</th>
                            <th class="centered">No. of Filled Up Posts</th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php if (empty($facilityPosts)): ?>

                        <tr>
                            <td colspan="3">No post is assigned.</td>
                        </tr>
                        
                    <?php else : foreach ($facilityPosts AS $facilityPost) : ?>

                        <tr>
                            <td><?php echo $facilityPost['designation'] ?></td>
                            <td class="centered"><?php echo $facilityPost['no_of_sanctioned_posts'] ?></td>
                            <td class="centered"><?php echo $facilityPost['no_of_filled_up_posts'] ?></td>
                        </tr>

                    <?php endforeach; endif ?>
                    </tbody>
                </table>
            </span>

        </fieldset>
        
        <div style="padding: 5px 15px 30px 20px; font-size: 14px;">
            <a href="javascript:history.go(-1)" style="padding: 0 30px">Click here to go back</a>
        </div>

    </div>		<!-- .block_content ends -->

</div>		<!-- .block ends -->