<div class="block">

    <?php if (!empty($errorMessage)) : ?>
    <div class="message errormsg">
        <?php echo $errorMessage ?>
    </div>
    <?php endif ?>

    <div class="block_head">
        <h2><?php echo $facilityName; ?></h2>
    </div>

    <div class="block_content">
        
            <p> Date                   : <?php echo $report['created_date'] ?> </p>
            <p>  Sanction Posts         : <?php echo $report['no_of_sanction_posts'] ?>   </p>
            <p>  Filled Up Posts        : <?php echo $report['no_of_doctors'] ?>   </p>
            <p>  Total Presences        : <?php echo $report['no_of_doctor_present'] ?>   </p>
            <p>  Total Absences         : <?php echo $report['no_of_doctor_absent'] ?>   </p>
            <p>  Authorized Absences    : <?php echo $report['no_of_authorized_absence'] ?>  </p>
            <p>  Un-authorized Absences : <?php echo $report['no_of_unauthorized_absence'] ?>   </p>
            
            <br/>
            <br/>
            
            <p>                
                <input type="button" value ="Logout" class="submit small" onClick = "window.location = '<?php echo site_url('attendances/users/logout')?>'" />
            </p>
            
    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->