<div class="block">

    <?php if (!empty($errorMessage)) : ?>
    <div class="message errormsg">
        <?php echo $errorMessage ?>
    </div>
    <?php endif ?>

    <div class="block_head">
        <h2>Edit Organization</h2>
    </div>

    <div class="block_content">
        
        <form action="" method="POST">

            <p>
                <label for="name">
                    Organization Name: <span class="required">*</span>
                </label>

                <input id="name" type="text" name="name" class="text medium"
                       value= "<?php echo set_value('name', $facility['name'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('name') ?>
                </span>
            </p>

            <p>
                <label for="chairperson">Head of Organization: </label>

                <input id="chairperson" type="text" name="chairperson" class="text small"
                       value= "<?php echo set_value('chairperson', $facility['chairperson'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('chairperson') ?>
                </span>
            </p>
            
             <p>
                <label for="local_id">Local ID:</label>
                <input id="local_id" type="text" name="local_id" class="text small"
                       value= "<?php echo $facility['local_id'] ?>" /><br />
                <span class='note error'>&nbsp;</span>
            </p>

            <p>
                <label for="division_id">
                    Division: <span class="required">*</span>
                </label>

                <select id="division_id" name="division_id" class="styled">
                    <option value=''>- Select Division -</option>

                    <?php foreach ($divisions as $division) : ?>
                        <option value="<?php echo $division['division_id'] ?>"
                            <?php echo ($division['division_id'] == $facility['division_id']) ? 'selected="selected"' : '' ?>>
                            <?php echo $division['name'] ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('division_id') ?>
                </span>
            </p>

            <p>
                <label for="district_id">
                    District: <span class="required">*</span>
                </label>

                <select id="district_id" name="district_id" class="styled">
                    <option value=''>- Select District -</option>

                    <?php foreach ($districts as $district) : ?>
                        <option value="<?php echo $district['district_id'] ?>"
                            <?php echo ($district['district_id'] == $facility['district_id']) ? 'selected="selected"' : '' ?>>
                            <?php echo $district['name'] ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('district_id') ?>
                </span>
            </p>

            <p>
                <label for="upazilla_id">
                    Upazila: <span class="required">*</span>
                </label>

                <select id="upazilla_id" name="upazilla_id" class="styled">
                    <option value=''>- Select Upazila -</option>

                    <?php foreach ($upazillas as $upazilla) : ?>
                        <option value="<?php echo $upazilla['upazilla_id'] ?>"
                            <?php echo ($upazilla['upazilla_id'] == $facility['upazilla_id']) ? 'selected="selected"' : '' ?>>
                            <?php echo $upazilla['name'] ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('upazilla_id') ?>
                </span>
            </p>

            <p>
                <label for="union_id">
                    Union: <span class="required">*</span>
                </label>

                <select id="union_id" name="union_id" class="styled">
                    <option value=''>- Select Union -</option>

                    <?php foreach ($unions as $union) : ?>
                        <option value="<?php echo $union['union_id'] ?>"
                            <?php echo ($union['union_id'] == $facility['union_id']) ? 'selected="selected"' : '' ?>>
                            <?php echo $union['name'] ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('union_id') ?>
                </span>
            </p>

            <p>
                <label for="type_id">
                    Type: <span class="required">*</span>
                </label>

                <select id="type_id" name="type_id" class="styled">
                    <option value=''>- Select Type -</option>

                    <?php foreach ($types as $type) : ?>
                        <option value="<?php echo $type['type_id'] ?>"
                            <?php echo ($type['type_id'] == $facility['type_id']) ? 'selected="selected"' : '' ?>>
                            <?php echo $type['name'] ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('type_id') ?>
                </span>
            </p>

            <p>
                <label for="email_address">
                    Email Address: <span class="required">*</span>
                </label>

                <input id="email_address" type="text" name="email_address" class="text small"
                       value= "<?php echo set_value('email_address', $facility['email_address'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('email_address') ?>
                </span>
            </p>

            <p>
                <label for="contact_no">
                    Contact No.:
                </label>

                <input id="contact_no" type="text" name="contact_no" class="text small"
                       value= "<?php echo set_value('contact_no', $facility['contact_no'])?>" /><br />
                <span class='note error'>
                    <?php echo form_error('contact_no') ?>
                </span>
            </p>

            <p>
                <input type="submit" value="Save" class="submit small" />
                <input type="hidden" name="facility_id" value="<?php echo $facility['facility_id'] ?>" />
                <input type="button" value ="Exit" class="submit small" onClick = "window.location = '<?php echo site_url('attendances/facilities/index')?>'" />
            </p>

        </form>

    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->