
    <div class="block_content">
        
        <form action="<?php echo site_url('attendances/facilities/test') ?>" method="POST">
            
         <table>             
                <p>
                    <label for="event">
                        Event: <span class="required">*</span>
                    </label>

                    <select id="job_class" name="event" class="styled">
                        <option value=''>- Select Event -</option>

                        <?php foreach ($this->config->item('event') as $key => $row) : ?>
                            <option value="<?php echo $key ?>"
                                <?php echo set_select('event', $key) ?>>
                                <?php echo $row ?></option>                            
                        <?php endforeach ?>
                            
                    </select>
                    
                    <span class='note error'>
                        <?php echo form_error('event') ?>
                    </span>
                    
                </p>

                <p>
                    <label for="name">
                        Comment: <span class="required">*</span>
                    </label>

                    <input id="name" type="text" name="name" class="text small" value= "<?php echo set_value('name')?>" /><br />
                    <span class='note error'>
                        <?php echo form_error('name') ?>
                    </span>
                </p>     
                
            </table>       
            
            <p><input type="submit" value="Submit" class="submit small" style="float:right;"/></p>
        </form>

<!--        <div class="pagination right">-->
<!--            --><?php //echo $this->pagination->create_links() ?>
<!--        </div> <!--.pagination ends--><!-- -->

    </div> <!--.block_content ends-->

</div> <!--.block ends-->
