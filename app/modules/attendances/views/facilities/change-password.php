<div class="block">

    <?php if (!empty($errorMessage)) : ?>
    <div class="message errormsg">
        <?php echo $errorMessage ?>
    </div>
    <?php endif ?>

    <div class="block_head">
        <h2>Organization Name: <?php echo $facilityName ?></h2>
    </div>

    <div class="block_content">
        
        <form action="" method="POST">

            <p>
                <label for="current-password">
                    Current Password: <span class="required">*</span>
                </label>

                <input id="current-password" type="password" name="current_password" class="text small"
                       value= "<?php echo set_value('current_password') ?>" /><br />
                <span class='note error'>
                    <?php echo form_error('current_password') ?>
                </span>
            </p>

            <p>
                <label for="new-password">
                    New Password: <span class="required">*</span>
                </label>

                <input id="new-password" type="password" name="new_password" class="text small"
                       value= "<?php echo set_value('new_password') ?>" /><br />
                <span class='note error'>
                    <?php echo form_error('new_password') ?>
                </span>
            </p>
            
            <p>
                <label for="confirmedPassword">
                    Confirmed Password: <span class="required">*</span>
                </label>

                <input id="confirmedPassword" type="password" name="confirmedPassword"
                       class="text small" value= "<?php echo set_value('confirmedPassword') ?>" /><br />
                <span class='note error'>
                    <?php echo form_error('confirmedPassword') ?>
                </span>
            </p>
            
            <p>
                <input type="submit" value="Save" class="submit small" />               
            </p>

        </form>

    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->