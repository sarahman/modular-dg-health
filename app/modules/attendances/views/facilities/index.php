<div class="block">

    <div class="block_head">
        <h2>Presence of <?php echo $facilityName ?> </h2>
        <ul>
            <li><a href="<?php echo site_url('attendances/staffs/add') ?>">Add Staff</a></li>
        </ul>
    </div> <!--.block_head ends -->

    <div class="block_content">
        <form action="<?php echo site_url('attendances/facilities/index') ?>" method="POST">

            <table cellpadding="0" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th rowspan="2" class="centered">Staff's Name</th>
                    <th rowspan="2" class="centered">Department</th>
                    <th rowspan="2" class="centered">Designation</th>
                    <th rowspan="2" class="centered">Present</th>
                    <th colspan="2" class="centered">Absent</th>
                    <th rowspan="2" class="centered">Other</th>
                    <th rowspan="2" class="centered">Comments</th>
                </tr>
                <tr>
                    <th class="centered shorter">Authorized</th>
                    <th class="centered shorter">Un-authorized</th>
                </tr>
                </thead>

                <tbody>
                <?php if (empty($doctors)) : ?>

                <tr>
                    <td colspan="8" class="nodatamsg">Data has not been found</td>
                </tr>

                <?php else : $count = 0; foreach($doctors as $facility) : ++$count; ?>

                <tr id ="<?php echo 'row_'.$count; ?>">
                    
                    <td class="centered"><?php echo $facility['name'] ?></td>
                    <td class="centered"><?php echo $facility['department_name'] ?></td>
                    <td class="centered"><?php echo $facility['designation'] ?></td>
                    <td class="centered shorter">
                        <input type="radio" name="statuses[<?php echo $count ?>]" value="1"
                               rel="<?php echo $count ?>" class="present" checked="checked" />
                    </td>
                    <td class="centered shorter">
                        <input type="radio" name="statuses[<?php echo $count ?>]" value="2"
                               rel="<?php echo $count ?>" class="authorized-absent"
                                <?php echo ( in_array($facility['staff_id'], $staffIds) )? "checked = 'checked'":"" ?> />
                    </td>
                    <td class="centered shorter">
                        <input type="radio" name="statuses[<?php echo $count ?>]" value="3"
                               rel="<?php echo $count ?>" class="un-authorized-absent" />
                    </td>
                    <td class="centered shorter">
                        <input type="radio" name="statuses[<?php echo $count ?>]" value="4"
                               rel="<?php echo $count ?>" class="transfer" />
                    </td>
                    <td class="centered">
                        <input type="text" name="comments[<?php echo $count ?>]"
                               rel="<?php echo $count ?>" id="comment_<?php echo $count ?>" style="display: none" />

                        <input type="hidden" name="doctorIds[<?php echo $count ?>]"
                               value="<?php echo $facility['staff_id'] ?>" id = "staffId_<?php echo $count ?>" />
                        <input type="hidden" name="date[<?php echo $count ?>]"
                               rel="<?php echo $count ?>" id="date_<?php echo $count ?>" />
                        <input type="hidden" name="event[<?php echo $count ?>]"
                               rel="<?php echo $count ?>" id="event_<?php echo $count ?>" />
                    </td>
                </tr>

                <?php endforeach; endif ?>
                </tbody>

            </table>
            <p>
                <input type="submit" value="Submit" class="submit small" style="float: right;" />
            </p>
        </form>
    </div> <!--.block_content ends-->

<!--        <div class="pagination right">-->
<!--            --><?php //echo $this->pagination->create_links() ?>
<!--        </div> <!--.pagination ends--><!-- -->

</div> <!--.block ends-->

<style type="text/css">
    .shorter { width: 5px !important; }
</style>


<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/js/facebox/facebox.css')?>" />
<script type="text/javascript" src="<?php echo site_url('assets/js/facebox/facebox.js')?>"></script>

<script type="text/javascript">

    var r = ''; var value = '';

    $(".authorized-absent").live('click', function(){
        showAuthorizeFaceBox(this)
    });

    $(".un-authorized-absent").live('click', function(){
        showCommentText(this);
    });

    $(".present").live('click', function(){
        hideCommentText(this);
    });

    $(".transfer").live('click', function(){
        showFaceBox(this);        
    });

    function showCommentText(el) {
        var rel = $(el).attr('rel');
        $("#comment_"+rel).show();
    }

    function hideCommentText(el) {
        var rel = $(el).attr('rel');
        $("#comment_"+rel).hide();
    }
    
    function showFaceBox(el) {
        r = $(el).attr('rel');
        hideCommentText(this);
        $.facebox({ajax: "<?php echo site_url('attendances/admin/absentEvent') ?>" });
    }

    function showAuthorizeFaceBox(el) {
        value = $(el).attr('rel');
        $.facebox({ajax: "<?php echo site_url('attendances/staffs/authorizeAbsences') ?>" });
    }

</script>