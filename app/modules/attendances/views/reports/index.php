<div class="block">

    <div class="block_head">
        <h2>Report</h2>
        <form method="POST" action="">
            <?php if (!empty($showDay)) : ?>

            <input type="text" name="date" class="text date_picker"
                   value="<?php echo empty($filters['date']) ? 'Enter Date' : mysql_to_human($filters['date']) ?>" />

            <?php else : ?>

                <select name ="year">
                    <option value="">- Select Year -</option>
                    <?php for($current = date("Y"); $current >= $firstYear; --$current): ?>
                    <option value="<?php echo $current ?>"
                        <?php echo (!empty($filters['year']) && $filters['year'] == $current) ? "selected = 'selected'" : '' ?>>
                        <?php echo $current ?></option>
                    <?php endfor ?>
                </select>

                <?php if (!empty($showMonth)) : ?>
                <select name ="month">
                    <option value="">- Select Month -</option>
                    <?php foreach ($months as $month): ?>
                    <option value="<?php echo $month['index'] ?>"
                        <?php echo (!empty($filters['month']) && $filters['month'] == $month['index']) ? "select = 'selected'" : '' ?>>
                        <?php echo $month['name'] ?></option>
                    <?php endforeach; ?>
                </select>
                <?php endif ?>

            <?php endif ?>

            <select name="division_id" id="parent">
                <option value="">- Select Division</option>
                <?php if ($divisions) :
                foreach($divisions as $division) : ?>

                <option value="<?php echo $division['division_id'] ?>"
                    <?php echo (!empty($filters['division_id']) && $filters['division_id'] == $division['division_id']) ? "selected = 'selected'" : '' ?>>
                    <?php echo $division['name'] ?></option>

                <?php endforeach; endif ?>

            </select>
            <select id="child" name="district_id">
                <option value="">- Select District</option>
                <?php if ($districts) :
                foreach($districts as $district) : ?>

                <option class="sub_<?php echo $district['division_id'] ?>"
                    value="<?php echo $district['district_id'] ?>"
                    <?php echo (!empty($filters['district_id']) && $filters['district_id'] == $district['district_id']) ? "selected = 'selected'" : '' ?>>
                    <?php echo $district['name'] ?></option>

                <?php endforeach; endif ?>
            </select>

            <select id="grandson" name="upazilla_id">
                <option value="">- Select Upazilla</option>
                <?php if ($upazillas) :
                foreach($upazillas as $upazilla) : ?>

                <option class="sub_<?php echo $upazilla['district_id'] ?>"
                    value="<?php echo $upazilla['upazilla_id'] ?>"
                    <?php echo (!empty($filters['upazilla_id']) && $filters['upazilla_id'] == $upazilla['upazilla_id']) ? "selected = 'selected'": '' ?>>
                    <?php echo $upazilla['name'] ?></option>

                <?php endforeach; endif ?>

            </select>
            <input type ="submit" value ="Submit" />

        </form>

    </div> <!--.block_head ends -->

    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <th class="date">Date</th>
                <th>Organization</th>
                <th class="centered">Sanctioned Post</th>
                <th class="centered">Filled Up Post</th>
                <th class="centered">Absent Authorized</th>
                <th class="centered">Absent Un-authorized</th>
            </tr>

            <?php if ($facilities) : foreach($facilities as $facility) : ?>
            <tr>
                <td><?php echo mysql_to_human($facility['created_date']) ?></td>
                <td><a href="<?php echo (site_url('attendances/reports/searchFacilityByID')."/facilityId/{$facility['facility_id']}/{$date}") ?>" ><?php echo $facility['name'] ?></a></td>
                <td class="centered"><?php echo $facility['no_of_sanction_posts'] ?></td>
                <td class="centered"><?php echo $facility['no_of_doctors'] ?></td>
                <td class="centered"><?php echo $facility['no_of_authorized_absence'] ?></td>
                <td class="centered"><?php echo $facility['no_of_unauthorized_absence'] ?></td>
            </tr>

            <?php endforeach; else : ?>

            <tr>
                <td colspan="6" class="nodatamsg">Data has not been found</td>
            </tr>
            
            <?php endif ?>

        </table>

        <div class="pagination right">
            <?php echo $this->pagination->create_links() ?>
        </div> <!--.pagination ends-->

    </div> <!--.block_content ends-->

</div> <!--.block ends-->

<script type="text/javascript" src="<?php echo site_url('assets/js/search-facility.js')?>"></script>
<script type="text/javascript">
    var selectedDistrict = "<?php echo (!empty($filters['district_id'])) ? $filters['district_id'] : 0 ?>";
    var selectedUpazilla = "<?php echo (!empty($filters['upazilla_id'])) ? $filters['upazilla_id'] : 0 ?>";
</script>