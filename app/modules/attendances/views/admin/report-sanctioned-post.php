<div class="block">

    <div class="block_head">
        
        <h2>All Facilities</h2>

    </div> <!--.block_head ends -->
    
    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">
            
            <thead>
                
                <tr>
                    <th>Organization Name</th>
                    <th>Action</th>
                </tr>            
                
            </thead>
            
            <tbody>                
            <?php if (empty($facilities)) : ?>
                
                <tr><td colspan="3" class="nodatamsg">No data is found.</td></tr>
            
                <?php else : foreach($facilities as $facility) : ?>
                <tr>                   
                    <td> <?php echo $facility['name'] ?>  </td>                
                    <td>
                        <a href="<?php echo site_url("attendances/admin/getReportByFacilityId/{$facility['facility_id']}") ?>" >Posts' Details</a> |
                        <a href="<?php echo site_url("attendances/admin/addSanctionPost/{$facility['facility_id']}") ?>" >Add Sanctioned Post</a>
                    </td>
                </tr>
               <?php endforeach; endif; ?>            
                
            </tbody>
            
        </table>

        <div class="pagination right">
            <?php echo $this->pagination->create_links() ?>
        </div> <!--.pagination ends-->

    </div> <!--.block_content ends-->

</div> <!--.block ends-->