<div class="block small" id="absent">

    <?php if (!empty($errorMessage)) : ?>
    <div class="message errormsg">
        <?php echo $errorMessage ?>
    </div>
    <?php endif ?>

    <div class="block_head">
        <h2>Event Description</h2>
    </div>

    <div class="block_content">
        
       <form action="<?php echo site_url('attendances/admin/absentEvent') ?>"
              id="add-event-form" method="POST">
            
             <p>
                <label for="eventBox">
                    Event: <span class="required">*</span>
                </label>

                <select id="eventBox" name="eventBox" >
                    <option value=''>- Select Event -</option>

                    <?php foreach ($this->config->item('event') as $key => $row) : ?>
                        <option value="<?php echo $key ?>" 
                                <?php echo set_select('eventBox', $key) ?>>
                            <?php echo $row ?></option>
                    <?php endforeach ?>

                </select>
                 <span class='note error'>
                    <?php echo form_error('eventBox') ?>
                </span>
            </p>

            <p>
                <label for="comment">
                    Comment: <span class="required">*</span>
                </label>
                
                <input id='comment' type="text" name="comment" class="text small"  value= "<?php echo set_value('comment') ?>" /><br />
                
                 <span class='note error'>
                    <?php echo form_error('comment') ?>
                </span>
            </p> 
            
            <p>
                <label for="date">
                    Date: <span class="required">*</span>
                </label>

                <input id="date" type="text" name="date" class="text date_picker" value= "<?php echo set_value('date')?>" /><br />
                <span class='note error'>
                    <?php echo form_error('date') ?>
                </span>
            </p>
            
            <p>                 
                <input type="submit" value="Save" id="submit-event" class="submit small" />
                <input type="button" value="Cancel" id="cancel" class="submit small" />
            </p>
            
        </form>

    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->

<script type="text/javascript" src="<?php echo site_url('assets/js/jquery.date_input.pack.js') ?>"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/custom.js') ?>"></script>
<script language="javascript" src="<?php echo site_url('assets/js/facebox/facebox.js')?>"></script>
<script type="text/javascript">
    $(function(){
    
        $('#submit-event').live('click', function(){

            $.ajax({
                url: $("#add-event-form").attr('action'),
                type: 'POST',
                data: {
                    eventBox: $('#eventBox').val(),
                    date: $('#date').val(),
                    comment: $('#comment').val()
                },
                success: function(content) {
                    if (content.substr(0, 4) == 'true') {

                        var data = content.substr(4);
                        data = data.split("|");
                        
                        $("#event_"+r).val(data[0]);
                        $("#comment_"+r).val(data[1]); 
                        $("#date_"+r).val(data[2]);
                        
                        $("#row_"+r).hide();
                        r = "";
                        
                        return closeFacebox();
                    } else {
                        $('#absent').replaceWith(content);
                    }
                }
            });

            return false;
        });

        $('#cancel').live('click', function(){
            return closeFacebox();
        });
    });

    function closeFacebox() {
        $.facebox.close();
        return false;
    }

</script>