<div class="block small" id="add-post-block">

    <?php if (!empty($errorMessage)) : ?>
    <div class="message errormsg">
        <?php echo $errorMessage ?>
    </div>
    <?php endif ?>

    <div class="block_head">
        <h2>Add Sanction Post</h2>
    </div>

    <div class="block_content">
        
<!--        <form action="--><?php //echo site_url('attendances/admin/addPost/'.$facility_id ) ?><!--"-->
        <form action="<?php echo site_url('attendances/admin/addPost' ) ?>"
              id="add-post-form" method="POST">
<!--       <P><input type="hidden" id ="facility_id" name="facility_id" value="--><?php //echo $facility_id; ?><!--"></p>-->

            <p>
                <label for="department_id">
                    Department: <span class="required">*</span>
                </label>

                <select id="department_id" name="department_id" class="styled">
                    <option value=''>- Select Department -</option>

                    <?php foreach ($departments as $department) : ?>
                        <option value="<?php echo $department['department_id'] ?>"
                            <?php echo set_select('department_id', $department['department_id']) ?>>
                            <?php echo $department['name'] ?></option>
                    <?php endforeach ?>

                </select><br />
                <span class='note error'>
                    <?php echo form_error('department_id') ?>
                </span>
            </p>

            <p>
                <label for="category_id">
                    Category: <span class="required">*</span>
                </label>

                <select id="category_id" name="category_id" class="styled">
                    <option value=''>- Select Staff Category -</option>

                    <?php foreach ($categories as $category) : ?>
                        <option value="<?php echo $category['category_id'] ?>"
                            <?php echo set_select('category_id', $category['category_id']) ?>>
                            <?php echo $category['title'] ?></option>
                    <?php endforeach ?>

                </select><br />
                <span class='note error'>
                    <?php echo form_error('category_id') ?>
                </span>
            </p>

            <p>
                <label for="designation_id">
                    Designation: <span class="required">*</span>
                </label>

                <select id="designation_id" name="designation_id" class="styled">
                    <option value=''>- Select Designation -</option>

                    <?php foreach ($designations as $designation) : ?>
                        <option value="<?php echo $designation['designation_id'] ?>"
                            <?php echo set_select('designation_id', $designation['designation_id']) ?>>
                            <?php echo $designation['title'] ?></option>
                    <?php endforeach ?>

                </select><br />

                <span class='note error'>
                    <?php echo form_error('designation_id') ?>
                </span>
           </p>
            
            <p>
                <label for="no_of_sanctioned_posts">
                    Number of Sanction Posts: <span class="required">*</span>
                </label>

                <input type="text" name="no_of_sanctioned_posts" id="no_of_sanctioned_posts" class="text small"
                       value="<?php echo set_value('no_of_sanctioned_posts')?>" /><br />
                <span class='note error'>
                    <?php echo form_error('no_of_sanctioned_posts') ?>
                </span>
            </p>
            
            <p>
                <input type="submit" value="Save" id="submit" class="submit small" />
                <input type="button" value="Cancel" id="cancel" class="submit small" />
            </p>

        </form>

    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->

<script type="text/javascript">
    

    $(function() {

<!--        if ($('#category_id').val() != '') {-->
<!--            getDesignations($('#category_id').val())-->
<!--        }-->
<!--        $('#category_id').live('change', function(){-->
<!--            getDesignations($(this).val())-->
<!--        });-->
<!---->
<!--        function getDesignations(category) {-->
<!--            $.ajax({-->
<!--                url: '--><?php //echo site_url('ajax/getDesignationsList') ?><!--',-->
<!--                type: 'POST',-->
<!--                data: {'category_id': category, 'designation_id': $('#designation_id').val()},-->
<!--                success: function(options){-->
<!--                    $('#designation_id').html(options);-->
<!--                }-->
<!--            });-->
<!--        }-->


        $('#submit').live('click', function(){

            $.ajax({
                url: $("#add-post-form").attr('action'),
                type: 'POST',
                data: {
                    facility_id: $('#facility_id').html(),
                    department_id: $('#department_id').val(),
                    category_id: $('#category_id').val(),
                    designation_id: $('#designation_id').val(),
                    no_of_sanctioned_posts: $('#no_of_sanctioned_posts').val()
                },
                success: function(content) {
                    if (content.substr(0, 4) == 'true') {
<!---->
<!--                        $.ajax({-->
<!--                            url: '--><?php //echo site_url('ajax/getSanctionedPost') ?><!--',-->
<!--                            type: 'POST',-->
<!--                            data: {-->
<!--                                facility_designation_id: content.substr(4)-->
<!--                            },-->
<!--                            success: function(content) {-->
<!---->
<!--                                if (content == '') {-->
<!--                                    return closeFacebox();-->
<!--                                }-->
<!--                                var reg = new RegExp("[\n]+", "g");-->
<!--                                var keyPairs = content.split(reg);-->
<!--                                reg = RegExp("[=]+", "g");-->
<!--                                var len = keyPairs.length;-->
<!--                                var parts = new Array();-->
<!--                                var data = new Array();-->
<!--                                for (var i = 0; i < len; ++i) {-->
<!--                                    parts = keyPairs[i].split(reg);-->
<!--                                    data[parts[0]] = parts[1];-->
<!--                                }-->
<!---->
<!--                                updatePostRow(data);-->
<!--                            }-->
<!--                        });-->
                      window.location = '';

                        return closeFacebox();
                        
                    } else {
                        $('#add-post-block').replaceWith(content);
                    }
                }
            });

            return false;
        });

        $('#cancel').live('click', function(){
            return closeFacebox();
        });
    });

    function closeFacebox() {
        $.facebox.close();
        return false;
    }

    function updatePostRow(data) {
        $('#postCount').html(Number($('#postCount').html())+1);
        var newRow = $('<tr></tr>').attr('id', 'row_'+$('#postCount').html())
            .append(getPostColumn(data['department']))
            .append(getPostColumn(data['category']))
            .append(getPostColumn(data['designation']))
            .append(getPostColumn(data['no_of_sanctioned_posts']))
            .append(getPostEditColumn(data['facility_designation_id']));

        $('#posts-container').append(newRow);
    }

    function getPostColumn(value) {
        return $('<td></td>').addClass('centered').append(value);
    }

    function getPostEditColumn(value) {
        return $('<td></td>').addClass('centered')
            .append($('<a></a>').attr('href', '').attr('rel', value).html('Edit'));
    }

</script>