<div class="block">

    <?php if (!empty($errorMessage)) : ?>
    <div class="message errormsg">
        <?php echo $errorMessage ?>
    </div>
    <?php endif ?>

    <div class="block_head">
        <h2>Organization Name: <?php echo $facilityName ?></h2>
    </div>

    <div class="block_content">
        
        <form action="" method="POST">

            <p>
                <label for="password">
                    Password: <span class="required">*</span>
                </label>

                <input id="password" type="password" name="password" class="text small"
                       value= "<?php echo set_value('password') ?>" /><br />
                <span class='note error'>
                    <?php echo form_error('password') ?>
                </span>
            </p>
            
            <p>
                <label for="confirmedPassword">
                    Confirmed Password: <span class="required">*</span>
                </label>

                <input id="confirmedPassword" type="password" name="confirmedPassword"
                       class="text small" value= "<?php echo set_value('confirmedPassword') ?>" /><br />
                <span class='note error'>
                    <?php echo form_error('confirmedPassword') ?>
                </span>
            </p>
            
            <p>
                <input type="submit" value="Save" class="submit small" />               
            </p>

        </form>

    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->