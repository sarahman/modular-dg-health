<div class="block">

    <?php if (!empty($errorMessage)) : ?>
    <div class="message errormsg">
        <?php echo $errorMessage ?>
    </div>
    <?php endif ?>

    <div class="block_head">
        <h2>Add Organization</h2>
    </div>

    <div class="block_content">
        
        <form action="<?php echo site_url('attendances/admin/addFacility') ?>" method="POST">

            <p>
                <label for="name">
                    Organization Name: <span class="required">*</span>
                </label>

                <input id="name" type="text" name="name" class="text small"
                       value= "<?php echo set_value('name') ?>" /><br />
                <span class='note error'>
                    <?php echo form_error('name') ?>
                </span>
            </p>

            <p>
                <label for="local_id">Local ID:</label>
                <input id="local_id" type="text" name="local_id" class="text small"
                       value= "<?php echo $this->input->post('local_id') ?>" /><br />
                <span class='note error'>&nbsp;</span>
            </p>

            <p>
                <label for="division_id">
                    Division: <span class="required">*</span>
                </label>

                <select id="division_id" name="division_id" class="styled">
                    <option value=''>- Select Division -</option>

                    <?php foreach ($divisions as $division) : ?>
                        <option value="<?php echo $division['division_id'] ?>"
                            <?php echo set_select('division_id', $division['division_id']) ?>>
                            <?php echo $division['name'] ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('division_id') ?>
                </span>
            </p>

            <p>
                <label for="district_id">
                    District: <span class="required">*</span>
                </label>

                <select id="district_id" name="district_id" class="styled">
                    <option value=''>- Select District -</option>

                    <?php foreach ($districts as $district) : ?>
                        <option value="<?php echo $district['district_id'] ?>"
                            <?php echo set_select('district_id', $district['district_id']) ?>>
                            <?php echo $district['name'] ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('district_id') ?>
                </span>
            </p>

            <p>
                <label for="upazilla_id">
                    Upazila: <span class="required">*</span>
                </label>

                <select id="upazilla_id" name="upazilla_id" class="styled">
                    <option value=''>- Select Upazila -</option>

                    <?php foreach ($upazillas as $upazilla) : ?>
                        <option value="<?php echo $upazilla['upazilla_id'] ?>"
                            <?php echo set_select('upazilla_id', $upazilla['upazilla_id']) ?>>
                            <?php echo $upazilla['name'] ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('upazilla_id') ?>
                </span>
            </p>

            <p>
                <label for="type_id">
                    Type: <span class="required">*</span>
                </label>

                <select id="type_id" name="type_id" class="styled">
                    <option value=''>- Select Type -</option>

                    <?php foreach ($types as $type) : ?>
                        <option value="<?php echo $type['type_id'] ?>"
                            <?php echo set_select('type_id', $type['type_id']) ?>>
                            <?php echo $type['name'] ?></option>
                    <?php endforeach ?>

                </select>
                <span class='note error'>
                    <?php echo form_error('type_id') ?>
                </span>
            </p>

            <p>
                <label for="username">
                    Username: <span class="required">*</span>
                </label>

                <input id="username" type="text" name="username" class="text small"
                       value= "<?php echo set_value('username') ?>" /><br />
                <span class='note error'>
                    <?php echo form_error('username') ?>
                </span>
            </p>

            <p>
                <label for="password">
                    Password: <span class="required">*</span>
                </label>

                <input id="password" type="password" name="password" class="text small"
                       value= "<?php echo set_value('password') ?>" /><br />
                <span class='note error'>
                    <?php echo form_error('password') ?>
                </span>
            </p>

            <p>
                <label for="confirmedPassword">
                    Confirmed Password: <span class="required">*</span>
                </label>

                <input id="confirmedPassword" type="password" name="confirmedPassword"
                       class="text small" value= "<?php echo set_value('confirmedPassword') ?>" /><br />
                <span class='note error'>
                    <?php echo form_error('confirmedPassword') ?>
                </span>
            </p>

            <p>
                <input type="submit" value="Save" class="submit small" />
                <input type="button" value ="Exit" class="submit small"
                       onClick = "window.location='<?php echo site_url('attendances/admin/viewFacilities') ?>'" />
            </p>

        </form>

    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->