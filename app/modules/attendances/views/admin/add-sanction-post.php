<div class="block">

    <?php if (!empty($errorMessage)) : ?>
    <div class="message errormsg">
        <?php echo $errorMessage ?>
    </div>
    <?php endif ?>

    <div class="block_head">
        <h2>Organization Name: <?php echo $facility['name'] ?></h2>
        <ul>
            <li><a href="" id="new-post">New Sanction Post</a></li>
        </ul>
    </div>

    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="centered">Department</th>
                <th class="centered">Category</th>
                <th class="centered">Designation</th>
                <th class="centered">No. of Sanctioned Posts</th>
                <th class="centered">Action</th>
            </tr>
            </thead>

            <tbody id="posts-container">
            <?php if (empty($posts)) : ?>

            <tr>
                <td colspan="5" class="nodatamsg">Data has not been found</td>
            </tr>

            <?php else : $count = 0; foreach($posts as $post) : ++$count; ?>

            <tr id ="<?php echo 'row_'.$count; ?>">
                <td class="centered"><?php echo $post['department'] ?></td>
                <td class="centered"><?php echo $post['category'] ?></td>
                <td class="centered"><?php echo $post['designation'] ?></td>
                <td class="centered"><?php echo $post['no_of_sanctioned_posts'] ?></td>
                <td class="centered">
                    <a class="edit-post" href="" rel="<?php echo $post['facility_designation_id'] ?>">Edit</a>
                </td>
            </tr>

            <?php endforeach; endif ?>
            </tbody>

        </table>

        <div id="postCount" style="display: none;"><?php echo $count ?></div>
        <div id="facility_id" style="display: none;"><?php echo $facility['facility_id'] ?></div>
        <div id="edit-row-no" style="display: none;"></div>

    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->

<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/js/facebox/facebox.css')?>" />
<script language="javascript" src="<?php echo site_url('assets/js/facebox/facebox.js')?>"></script>

<script type="text/javascript">

    $(function() {

        $('#new-post').live('click', function(){
            $.facebox({ajax: '<?php echo site_url('attendances/admin/addPost') ?>' + '/' + $('#facility_id').html()});
            return false;
        });

        $('.edit-post').live('click', function(){
            var rowNo = $(this).parent().parent().attr('id').substr(4);
            $('#edit-row-no').html(rowNo);
            $.facebox({ajax: '<?php echo site_url('attendances/admin/editPost') ?>/'+$(this).attr('rel') });
            return false;
        });

    });

</script>