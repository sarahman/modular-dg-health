<div class="block">

    <div class="block_head">
        
        <h2>Organization: <?Php echo $facilityName; ?></h2>

    </div> <!--.block_head ends -->
    
    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">
            
            <thead>
                
                <tr>
                    <th>Department</th>
                    <th>Designation</th>
                    <th class="centered width">Sanctioned Post</th>
                    <th class="centered width">Filled Up Post</th>
                    <th class="centered">Vacant</th>
                </tr>            
                
            </thead>
            
            <tbody>                
            <?php if (empty($postDetails)) : ?>
                
                <tr><td colspan="3" class="nodatamsg">No data is found.</td></tr>
            
                <?php else : foreach($postDetails as $row) : ?>
                <tr>                   
                    <td> <?php echo $row['department'] ?> </td>
                    <td> <?php echo $row['designation'] ?> </td>
                    <td class="centered width"> <?php echo $row['no_of_sanctioned_posts'] ?> </td>
                    <td class="centered width"> <?php echo $row['no_of_filled_up_posts'] ?> </td>
                    <td class="centered width"> <?php echo ($row['no_of_sanctioned_posts'] - $row['no_of_filled_up_posts']) ?> </td>
                </tr>
               <?php endforeach; endif; ?>            
                
            </tbody>
            
        </table>

        <div class="pagination right">
            <?php //echo $this->pagination->create_links() ?>
        </div> <!--.pagination ends-->

    </div> <!--.block_content ends-->

</div> <!--.block ends-->
<style type="text/css">

    .width { width: 14% !important; }

</style>