
                    <ul id="nav">
                        <li><a href="<?php echo site_url('attendances/home') ?>">Home</a></li>
                        <li class="active"><a href=<?php echo site_url('attendances/search') ?>>Search</a>
                            <ul>
                                <li><a href=<?php echo site_url('attendances/search') ?>>Search by Location</a></li>
                                <li><a href=<?php echo site_url('attendances/search/byDate') ?>>Search by Date</a></li>
                                <li><a href=<?php echo site_url('attendances/search/byType') ?>>Search by Type</a></li>
                            </ul>
                        </li>
                        <li><a href=<?php echo site_url('attendances/reports') ?>>Report</a>
                            <ul>
                                <li><a href=<?php echo site_url('attendances/reports') ?>>Report by Date</a></li>
                                <li><a href=<?php echo site_url('attendances/reports/byMonth') ?>>Report by Month</a></li>
                                <li><a href=<?php echo site_url('attendances/reports/byYear') ?>>Report by Year</a></li>
                            </ul>
                        </li>
                        <li><a href=<?php echo site_url('attendances/summaries') ?>>Summary</a>
                            <ul>
                                 <li><a href=<?php echo site_url('attendances/summaries') ?>>No Submission</a></li>
                                 <li><a href=<?php echo site_url('attendances/summaries/viewPost') ?>>Sanction & Filled Post >></a>
                                     <ul>
                                         <li><a href=<?php echo site_url('attendances/summaries/viewPost') ?>>Any Post</a></li>
                                         <li><a href=<?php echo site_url('attendances/summaries/sumViewPost') ?>>All Posts</a></li>
                                         <li><a href=<?php echo site_url('attendances/summaries/typeViewPost') ?>>Posts By Type</a></li>

                                     </ul>
                                 </li>
                                 <li><a href=<?php echo site_url('attendances/summaries/viewAbsence') ?>>Attendance >></a>
                                     <ul>
                                         <li><a href=<?php echo site_url('attendances/summaries/viewAbsence') ?>>Any</a></li>
                                         <li><a href=<?php echo site_url('attendances/summaries/sumOfAbsences') ?>>All </a></li>
                                         <li><a href=<?php echo site_url('attendances/summaries/typeSumOfAbsences') ?>>By Type</a></li>

                                     </ul>
                                 </li>
                                 
                            </ul>                            
                        </li>
                        <li class="active"><a href=<?php echo site_url('attendances/admin/viewFacilities') ?>>Administration</a>
                            <ul>
                                <li><a href=<?php echo site_url('attendances/admin/viewFacilities') ?>>View All Facilities</a></li>
                                <li><a href=<?php echo site_url('attendances/admin/reportSanctionedPost') ?>>Sanctioned Posts</a></li>
                                <li><a href=<?php echo site_url('attendances/admin/addFacility') ?>>Add Organization</a></li>
                            </ul>
                        </li>
                    </ul>