<div class="block">

    <div class="block_head">
        <h2>Summary</h2>

        <form id="reportForm" method="POST" action="">
            <?php if (!empty($showType)): ?>

            <select name="type_id">
                <option value="">- Search by Type -</option>
                <?php foreach ($types as $type): ?>
                <option value="<?php echo $type['type_id'] ?>" <?php echo ($this->input->post('type_id') == $type['type_id']) ? "selected = 'selected'" : '' ?> >
                    <?php echo $type['name'] ?>
                </option>
                <?php endforeach; ?>
            </select>

            <input type="submit" value="Submit" />

            <?php endif; ?>
        </form>
    </div>
    <!--.block_head ends -->

    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <th class="centered">Sanctioned Posts</th>
                <th class="centered">Filled Up Posts</th>
                <th class="centered">% of Filled Up</th>
            </tr>

            <?php if ($facility) : ?>
            <tr>
                <td class="centered"><?php echo $facility['no_of_sanction_posts'] ?></td>
                <td class="centered"><?php echo $facility['no_of_doctors'] ?></td>
                <td class="centered"><?php echo getPercentage($facility['no_of_doctors'], $facility['no_of_sanction_posts']) ?></td>
            </tr>

            <?php endif ?>

        </table>

    </div>
    <!--.block_content ends-->

</div> <!--.block ends-->