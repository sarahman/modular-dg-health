<div class="block">

    <div class="block_head">
        <h2>summary</h2>
        <form method="POST" action="">
            
            <input type="text" name="startingDate" class="text date_picker"
                   value="<?php echo empty($filters['startingDate']) ? 'Enter Start Date' : mysql_to_human($filters['startingDate']) ?>" />

            <input type="text" name="endingDate" class="text date_picker"
                   value="<?php echo empty($filters['endingDate']) ? 'Enter End Date' : mysql_to_human($filters['endingDate']) ?>" />
            
            <input type ="submit" value ="Submit" />

        </form>

    </div> <!--.block_head ends -->

    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <th class="centered">Organization Name</th>
                <th class="centered">Email</th>
            </tr>

            <?php if ($facilities) : foreach($facilities as $facility) : ?>

            <tr>
                <td class="centered"><?php echo $facility['name'] ?></td>
                <td class="centered"><?php echo $facility['email_address'] ?></td>
            </tr>

            <?php endforeach; else : ?>

            <tr>
                <td colspan="6" class="nodatamsg">Data has not been found</td>
            </tr>

            <?php endif ?>

        </table>

        <div class="pagination right">
            <?php echo $this->pagination->create_links() ?>
        </div> <!--.pagination ends-->

    </div> <!--.block_content ends-->

</div> <!--.block ends-->
