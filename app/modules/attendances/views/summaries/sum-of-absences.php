<div class="block">

    <div class="block_head">
        <h2>Summary</h2>
        <form id="reportForm" method="POST" action="">

            <input type="text" name="startingDate" class="text date_picker"
                   value="<?php echo ($this->input->post('startingDate')) ? $this->input->post('startingDate') : 'Starting date' ?>" />

            <input type="text" name="endingDate" class="text date_picker"
                   value="<?php echo ($this->input->post('endingDate')) ? $this->input->post('endingDate') : 'Ending date' ?>" />
                
            <?php if (!empty($showType)) : ?>

                <select name="type_id" >
                    <option value="">- Search by Type -</option>
                    <?php foreach($types as $type): ?>
                    <option value ="<?php echo $type['type_id'] ?>" <?php echo ($this->input->post('type_id') == $type['type_id']) ? "selected = 'selected'" : '' ?> > <?php echo $type['name'] ?> </option>
                    <?php endforeach; ?>
                </select>

            <?php endif; ?>

            <input type ="submit" value ="Submit" />
        </form>
    </div> <!--.block_head ends -->
    
    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="centered" colspan="2">Filled Up Post</th>
                    <th class="centered" colspan="2">Total Absent</th>
                    <th class="centered" colspan="2">Absent Authorized</th>
                    <th class="centered" colspan="2">Absent Un-authorized</th>
                </tr>
                <tr>
                    <th class="centered">Average</th>
                    <th class="centered">Percentage</th>
                    <th class="centered">Average</th>
                    <th class="centered">Percentage</th>
                    <th class="centered">Average</th>
                    <th class="centered">Percentage</th>
                    <th class="centered">Average</th>
                    <th class="centered">Percentage</th>
                </tr>
            </thead>
            
            <tbody>

            <?php if (empty($staff)) : ?>
                
            <tr>
                <td colspan="8" class="nodatamsg">Data has not been found</td>
            </tr>

            <?php else : ?>
                
            <tr>
                <td class="centered"><?php echo number_format($staff['total_no_of_doctors']) ?></td>
                <td class="centered"><?php echo getPercentage($staff['total_no_of_doctors'], $staff['total_no_of_sanction_posts'])?></td>
                <td class="centered"><?php echo number_format(($staff['total_no_of_authorized_absence'] + $staff['total_no_of_unauthorized_absence'])/2) ?></td>
                <td class="centered"><?php echo getPercentage(($staff['total_no_of_authorized_absence'] + $staff['total_no_of_unauthorized_absence']), $staff['total_no_of_doctors']) ?></td>
                <td class="centered"><?php echo number_format($staff['total_no_of_authorized_absence']) ?></td>
                <td class="centered"><?php echo getPercentage($staff['total_no_of_authorized_absence'], $staff['total_no_of_doctors']) ?></td>
                <td class="centered"><?php echo number_format($staff['total_no_of_unauthorized_absence']) ?></td>
                <td class="centered"><?php echo getPercentage($staff['total_no_of_unauthorized_absence'], $staff['total_no_of_doctors']) ?></td>
            </tr>

            <?php endif ?>
                
            </tbody>
            
        </table>

    </div> <!--.block_content ends-->

</div> <!--.block ends-->