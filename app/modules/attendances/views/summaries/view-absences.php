<div class="block">

    <div class="block_head">

        <h2>Absent Status</h2>

        <form id="reportForm" method="POST" action="">
            
                <?php if (!empty($showDate)) : ?>
                <input type="text" name="startingDate" class="text date_picker"
                       value="<?php echo (empty($filters['startingDate'])) ? 'Starting date' : mysql_to_human($filters['startingDate']) ?>" />
                
                <input type="text" name="endingDate" class="text date_picker"
                       value="<?php echo (empty($filters['endingDate'])) ? 'Ending date' : mysql_to_human($filters['endingDate']) ?>" />
                <?php endif; ?>

            <input type ="submit" value ="Submit" />

        </form>

    </div> <!--.block_head ends -->

    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th rowspan="2">Organization</th>
                    <th class="centered" rowspan="2">Filled Up Post</th>
                    <th class="centered" colspan="2">Total Absent</th>
                    <th class="centered" colspan="2">Absent Authorized</th>
                    <th class="centered" colspan="2">Absent Un-authorized</th>
                </tr>
                <tr>
                    <th class="centered">Average</th>
                    <th class="centered">Percentage</th>
                    <th class="centered">Average</th>
                    <th class="centered">Percentage</th>
                    <th class="centered">Average</th>
                    <th class="centered">Percentage</th>
                </tr>
            </thead>
            
            <tbody>

                <?php if (empty($facilities)) : ?>

                <tr>
                    <td colspan="8" class="nodatamsg">Data has not been found</td>
                </tr>

                <?php else : foreach($facilities as $facility) : ?>
                    <tr>
                        <td><a href="<?php echo (site_url('attendances/search/searchFacilityByID')."/{$facility['facility_id']}/{$dateRange}") ?>"><?php echo $facility['name'] ?></a></td>
                        <td class="centered"><?php echo number_format($facility['total_no_of_doctors']) ?></td>
                        <td class="centered"><?php echo number_format(($facility['avg_no_of_authorized_absence'] + $facility['avg_no_of_unauthorized_absence'])/2) ?></td>
                        <td class="centered"><?php echo getPercentage(($facility['total_no_of_authorized_absence'] + $facility['total_no_of_unauthorized_absence']), $facility['total_no_of_doctors']) ?></td>
                        <td class="centered"><?php echo number_format($facility['avg_no_of_authorized_absence']) ?></td>
                        <td class="centered"><?php echo getPercentage($facility['total_no_of_authorized_absence'], $facility['total_no_of_doctors']) ?></td>
                        <td class="centered"><?php echo number_format($facility['avg_no_of_unauthorized_absence']) ?></td>
                        <td class="centered"><?php echo getPercentage($facility['total_no_of_unauthorized_absence'], $facility['total_no_of_doctors']) ?></td>
                    </tr>

                <?php endforeach; endif ?>
            </tbody>

        </table>

        <div class="pagination right">
            <?php echo $this->pagination->create_links() ?>
        </div> <!--.pagination ends-->

    </div> <!--.block_content ends-->

</div> <!--.block ends-->