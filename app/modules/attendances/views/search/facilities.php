<div class="block">

    <div class="block_head">
        
        <h2>Organization: <?php echo $facilityName ?> </h2>

    </div> <!--.block_head ends -->
    
    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th rowspan="2" >Absent On Date</th>
                <th colspan="3" style="text-align: center !important;">Staff's Detail</th>
            </tr>
            <tr>
                <th>Staff's Name</th>
                <th>Department</th>
                <th>Designation</th>
            </tr>
                
            </thead>
            
            <tbody>
                
            <?php if (empty($facilities)) : ?>
                
            <tr><td colspan="4" class="nodatamsg">No data is found.</td></tr>
            
            <?php else: foreach($facilities as $facility) : ?>
            
            <tr>
                <td><?php echo mysql_to_human($facility['created_date']) ?></td>
                
                <td colspan="3">
                    <table width ="100%">
                       <?php if (empty($facility['doctors'])) : ?>
                        
                        <tr>
                            <td colspan="3">No doctor was absent.</td>
                        </tr>
                        
                        <?php else: foreach($facility['doctors'] as $row) :?>
                        <tr>
                            <td><a href="<?php echo (site_url('attendances/search/getDoctorRecord')."/{$row['staff_id']}") ?>" ><?php echo $row['name'] ?></a></td>
                            <td><?php echo $row['department'] ?></td>
                            <td><?php echo $row['title'] ?></td>
                        </tr>
                          
                       <?php endforeach; endif; ?>
                    </table>
                
                </td>
            </tr>
            <?php endforeach; endif; ?>
            
            </tbody>
        </table>

        <div class="pagination right">
            <?php echo $this->pagination->create_links() ?>
        </div> <!--.pagination ends-->

    </div> <!--.block_content ends-->

</div> <!--.block ends-->