<div class="block">

    <div class="block_head">

        <h2>Search</h2>
        
        <form id="reportForm" method="POST" action="">
            <?php if (empty($showType)) : ?>
                <?php if (!empty($showDate)) : ?>
                <input type="text" name="startingDate" class="text date_picker"
                       value="<?php echo (empty($filters['startingDate'])) ? 'Starting date' : mysql_to_human($filters['startingDate']) ?>" />
                <input type="text" name="endingDate" class="text date_picker"
                       value="<?php echo (empty($filters['endingDate'])) ? 'Ending date' : mysql_to_human($filters['endingDate']) ?>" />
                <?php endif; ?>

                <select name="division_id" id="parent">

                    <option value="">- Select Division -</option>

                    <?php if ($divisions): foreach($divisions as $division) : ?>

                    <option value="<?php echo $division['division_id'] ?>" <?php echo (!empty($filters['division_id']) && $filters['division_id'] == $division['division_id']) ? "selected = 'selected'" : '' ?>>
                        <?php echo $division['name'] ?>
                    </option>

                    <?php endforeach; endif ?>

                </select>

                <select id="child" name="district_id">

                    <option value="">- Select District -</option>

                    <?php if ($districts): foreach($districts as $district) : ?>

                    <option class="sub_<?php echo $district['division_id'] ?>" value="<?php echo $district['district_id'] ?>" <?php echo (!empty($filters['district_id']) && $filters['district_id'] == $district['district_id']) ? "selected = 'selected'" : '' ?>>
                        <?php echo $district['name'] ?>
                    </option>

                    <?php endforeach; endif ?>

                </select>

                <select id="grandson" name="upazilla_id">

                    <option value="">- Select Upazilla -</option>

                    <?php if ($upazillas): foreach($upazillas as $upazilla) : ?>

                    <option class="sub_<?php echo $upazilla['district_id'] ?>" value="<?php echo $upazilla['upazilla_id'] ?>" <?php echo (!empty($filters['upazilla_id']) && $filters['upazilla_id'] == $upazilla['upazilla_id']) ? "select = 'selected'" : '' ?>>
                        <?php echo $upazilla['name'] ?>
                    </option>

                    <?php endforeach; endif ?>

                </select>
            <?php else: ?>

                <select name="type_id" >
                    <option value="">- Search by Type -</option>
                    <?php foreach($types as $type): ?>
                    <option value ="<?php echo $type['type_id'] ?>" <?php echo ($this->input->post('type_id') == $type['type_id']) ? "selected = 'selected'" : '' ?> > <?php echo $type['name'] ?> </option>
                    <?php endforeach; ?>
                </select>
                
            <?php endif; ?>
                
            <input type ="submit" value ="Submit" />

        </form>

    </div> <!--.block_head ends -->
    
    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <th class="date">Date</ th>
                <th>Organization</th>
                <th class="centered">Sanctioned Post</th>
                <th class="centered">Filled Up Post</th>
                <th class="centered">Absent Authorized</th>
                <th class="centered">Absent Un-authorized</th>
            </tr>
            
            <?php if ($facilities) : foreach($facilities as $facility) : ?>
            
            <tr>
                <td><?php echo mysql_to_human($facility['created_date']) ?></td>
                <td><a href="<?php echo (site_url('attendances/search/searchFacilityByID')."/facilityId/{$facility['facility_id']}/{$dateRange}") ?>"><?php echo $facility['name'] ?></a></td>
                <td class="centered"><?php echo $facility['no_of_sanction_posts'] ?></td>
                <td class="centered"><?php echo $facility['no_of_doctors'] ?></td>
                <td class="centered"><?php echo $facility['no_of_authorized_absence'] ?></td>
                <td class="centered"><?php echo $facility['no_of_unauthorized_absence'] ?></td>
            </tr>

            <?php endforeach; else : ?>

            <tr>
                <td colspan="6" class="nodatamsg">Data has not been found</td>
            </tr>
            
            <?php endif ?>

        </table>

        <div class="pagination right">
            <?php echo $this->pagination->create_links() ?>
        </div> <!--.pagination ends-->

    </div> <!--.block_content ends-->

</div> <!--.block ends-->

<script type="text/javascript" src="<?php echo site_url('assets/js/search-facility.js')?>"></script>
<script type="text/javascript">
    var selectedDistrict = "<?php echo (!empty($filters['district_id'])) ? $filters['district_id'] : 0 ?>";
    var selectedUpazilla = "<?php echo (!empty($filters['upazilla_id'])) ? $filters['upazilla_id'] : 0 ?>";
</script>