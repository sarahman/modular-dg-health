<div class="block">

    <div class="block_head">
        
        <h2>Name: <?php echo $doctor['name']." (".$doctor['title'].", ".$doctor['department'].")"  ?> </h2>

    </div> <!--.block_head ends -->
    
    <div class="block_content">

        <table cellpadding="0" cellspacing="0" width="100%">
            
            <thead>
                
                <tr>
                    <th>Date</th>
                    <th>Reason</th>
                    <th>Authorized</th>
                </tr>            
                
            </thead>
            
            <tbody>                
            <?php if (empty($records)) : ?>
                
                <tr><td colspan="3" class="nodatamsg">No data is found.</td></tr>
            
                <?php else: foreach($records as $record) : ?>            
                <tr>
                    <td><?php echo mysql_to_human($record['created_date']) ?></td>
                    <td> <?php echo $record['reason'] ?>  </td>                
                    <td> <?php echo ($record['is_authorized'] == 0)? 'Y':'N'; ?>  </td>
                </tr>
               <?php endforeach; endif; ?>            
                
            </tbody>
            
        </table>

        <div class="pagination right">
            <?php echo $this->pagination->create_links() ?>
        </div> <!--.pagination ends-->

    </div> <!--.block_content ends-->

</div> <!--.block ends-->