<?php

/**
 * Description of Reports Controller
 *
 * @package     Controller   
 * @author      Eftakhairul
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class ReportsController extends BaseController
{
    public function __construct ()
    {
        parent::__construct();
        $this->ensureLoggedIn();
        $this->load->model('attendance');
        $this->load->model('absence');
        //////////////////////////////////
        $this->load->model('location');
        $this->load->model('facility');
        $this->load->library('pagination');
    }

    public function index()
    {
        $this->load->helper('date');
        $uri = site_url('attendances/reports/index');
        $this->initialization($uri, $_POST);
        
        $this->data['date'] = '';        
        if (!empty( $this->data['filters']['date'])) {
            $this->data['date'] = 'startingDate/' . human_to_mysql($this->data['filters']['date'])
                    . '/endingDate/' . human_to_mysql($this->data['filters']['date']);
        }
        
        $this->data['showDay'] = true;
        $this->layout->view('attendances/reports/index', $this->data);
    }

    public function byMonth()
    {
        $this->load->helper('date');
        $uri = site_url('attendances/reports/byMonth');
        
        $this->initialization($uri, $_POST);
        
        $this->data['firstYear'] = $this->attendances->getFirstYear();
        $this->data['months'] = getMonthNames();
        $this->data['date'] = '';
        $this->data['showMonth'] = true;
        
        $this->layout->view('attendances/reports/index', $this->data);
    }

    public function byYear()
    {
        $uri = site_url('attendances/reports/byYear');

        $this->initialization($uri, $_POST);
        
        $this->data['firstYear'] = $this->attendances->getFirstYear();
        $this->data['date'] = '';
   
        $this->layout->view('attendances/reports/index', $this->data);
    }    

    public function viewPost()
    {
        $uri = site_url('attendances/reports/viewPost');

        $this->initialization($uri, $_POST);
        $this->data['date'] = '';

        $this->layout->view('attendances/reports/post', $this->data);
    }
    
    public function searchFacilityByID()
    {
        $page = 0;
        $uriAssoc = $this->uri->uri_to_assoc();
        $url = site_url('attendances/reports/searchFacilityByID');
        
        if (!empty($uriAssoc['facilityId'])) {
            
            if (!empty($uriAssoc['page'])) {
                $page = $uriAssoc['page'];
                unset($uriAssoc['page']);
            }
                
            $this->data['facilities'] = $this->absences->searchByID($uriAssoc, $page);
            $this->data['facilityName'] = $this->facilities->getName($uriAssoc['facilityId']);
            
            $url .= $this->uri->assoc_to_uri($uriAssoc);     
            
            $paginationOptions = array(
                'baseUrl' => $url. '/page/',
                'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
                'numRows' => $this->absences->countRows($uriAssoc)
            );
           $this->pagination->setOptions($paginationOptions);            
        } else {
            $this->data['error'] = "Sorry, information is not found.";
        }
        
        $this->layout->view('attendances/search/facilities', $this->data);
    }

    private function getLocations()
    {
        $this->data['divisions'] = $this->locations->getAllDivisions();
        $this->data['districts'] = $this->locations->getAllDistricts();
        $this->data['upazillas'] = $this->locations->getAllUpazillas();
    }

    private function initialization($url, $data = array())
    {
        $page = 0;        
        $uriAssoc = $this->uri->uri_to_assoc();

        if (!empty($data)) {
            if (!empty($data['date'])) {
                $this->load->helper('date');
                $data['date'] = human_to_mysql($data['date']);
            }

            $this->load->helper('refactor');
            $data = eliminateNullField($data);

            $url .= $this->uri->assoc_to_uri($data);
            redirect($url);
        }

        else if (!empty($uriAssoc)) {
            $page = empty($uriAssoc['page']) ? 0 : $uriAssoc['page'];
            unset($uriAssoc['page']);

            $url .= $this->uri->assoc_to_uri($uriAssoc);
        }

        $this->getLocations();
        $this->data['facilities'] = $this->attendances->search($uriAssoc, $page);
        $this->data['filters'] = $uriAssoc;

        $paginationOptions = array(
            'baseUrl' => $url . '/page/',
            'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
            'numRows' => $this->attendances->countRows($uriAssoc)
        );

        $this->pagination->setOptions($paginationOptions);
    }
}
