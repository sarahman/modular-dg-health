<?php

/**
 * Description of Ajax Controller
 *
 * @package     Controller
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class AjaxController extends BaseController
{
    public function getDesignationsList()
    {
        $this->load->model('designation');

        $designations = $this->designations->getAllByCategory($this->input->post('category_id'));
        $output = "<option value=''>- Select Designation -</option>";

        if ($this->input->post('designation_id')) {
            $designationId = $this->input->post('designation_id');
            foreach ($designations AS $designation) {
                $output .= "<br /><option value='{$designation['designation_id']}'";
                $output .= ($designation['designation_id'] === $designationId) ? ' selected="selected">' : '>';
                $output .= "{$designation['title']}</option>";
            }
        } else {
            foreach ($designations AS $designation) {
                $output .= "<br /><option value='{$designation['designation_id']}'>
                    {$designation['title']}</option>";
            }
        }

        echo $output;
        return;
    }

    public function getSanctionedPost()
    {
        $this->load->model('facility_designation');
        $detail = $this->facilitiesdesignations->
            getSanctionedPost($this->input->post('facility_designation_id'));

        if (empty($detail)) {
            return;
        }

        foreach($detail AS $key => $value) {
            echo "{$key}={$value}\n";
        }
    }
}