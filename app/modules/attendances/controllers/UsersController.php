<?php

/**
 * Description of Users
 * @author      Eftakhairul
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class UsersController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user');
    }

    public function index()
    {
        $this->checkUserSession();
        $this->load->library('form_validation');
        $this->form_validation->setRulesForSignIn();

        if (!empty($_POST)) {

            if ($this->form_validation->run()) {

                $_POST['type'] = 'user';

                $result = $this->users->validateUser($_POST);

                if ($result) {

                    $this->session->set_userdata('username', $result['username']);
                    $this->session->set_userdata('userType', $result['type']);
                    $this->session->set_userdata('facilityId', $result['facility_id']);
                    $this->redirectForSuccess('attendances/facilities', 'You have successfully logged in.');

                } else {
                    $this->data['error'] = 'Enter correct Username & Password.';
                }

            } else {
                $this->data['error'] = 'Enter required information.';
            }
        }

        $this->load->view('attendances/users/index', $this->data);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('user');
    }

    protected function checkUserSession()
    {
        if ($this->session->userdata('userType') == 'user') {
            redirect('facilities');
        } else if ($this->session->userdata('userType') == 'admin') {
            redirect('home');
        }
    }
}