<?php

/**
 * Facilities Controller
 *
 * @package     Controller
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 * @author      Eftakhairul Islam <eftakhairul@gmail.com>
 */
class FacilitiesController extends BaseController
{
    protected $facilityId;
  
    public function __construct()
    {
        parent::__construct();
        $this->load->model('facility');
        $this->load->model('department');
        $this->load->model('category');
        $this->load->model('designation');
        $this->load->model('authorized_absence');
    }

    public function index()
    {
        $this->load->helper('date');
        $this->checkFacility();
        $this->load->model('staff');

        if (!empty($_POST)) {

            $this->load->model('absence');
            $this->load->model('attendance');
            $this->load->model('facility_designation');

            $presenceCount = 0;
            $authorizedAbsenceCount = 0;
            $unauthorizedAbsenceCount = 0;
            $count = 0;
            $staffIds = $this->input->post('doctorIds');
            $comments = $this->input->post('comments');
            $dates = $this->input->post('date');
            $events = $this->input->post('event');

            foreach($this->input->post('statuses') AS $row) {

                ++$count;
                switch($row) {
                    case 1:
                        ++$presenceCount;
                        break;

                    case 2:
                        ++$authorizedAbsenceCount;
                        $this->absences->save(array(
                            'facility_id' => $this->facilityId,
                            'staff_id' => $staffIds[$count],
                            'reason' => $comments[$count],
                            'is_authorized' => 1
                        ));
                        break;

                    case 3:
                        ++$unauthorizedAbsenceCount;
                        $this->absences->save(array(
                            'facility_id' => $this->facilityId,
                            'staff_id' => $staffIds[$count],
                            'reason' => $comments[$count],
                            'is_authorized' => 0
                        ));
                        break;

                    case 4:
                        $data = array(
                            "reason" => $comments[$count],
                            "status"   => $events[$count],
                            "event_date" => human_to_mysql($dates[$count])
                        );

                        $this->staffs->setAsRemoved($data, $staffIds[$count]);
                        $this->facilities->decrementStaffNumber($this->facilityId);
                        $postDetail = $this->staffs->getPostingDetail($staffIds[$count]);
                        $this->facilitiesdesignations->decrementFilledUpPostNumber($postDetail);
                        break;
                }
            }

            $this->attendances->save(array(
                'facility_id' => $this->facilityId,
                'no_of_doctor_present' => $presenceCount,
                'no_of_doctor_absent' => $authorizedAbsenceCount + $unauthorizedAbsenceCount,
                'no_of_authorized_absence' => $authorizedAbsenceCount,
                'no_of_unauthorized_absence' => $unauthorizedAbsenceCount
            ));

            $this->redirectForSuccess("attendances/facilities/report/{$this->facilityId}", 'The facilities data are saved successfully.');
        }

        $this->data['staffIds'] = $this->authorizeabsences->getAuthorizedStaffsByFacilityId($this->facilityId);
        $this->data['facilityName'] = $this->facilities->getName($this->facilityId);
        $this->data['doctors'] = $this->staffs->getDoctorsList($this->facilityId);
        $this->layout->view('attendances/facilities/index', $this->data);
    }

    public function report($facilityId)
    {
        $this->checkFacility();
        $this->load->model('attendance');
        $this->data['report'] = $this->attendances->getAbsencesByCurrentDate($facilityId);
        $this->data['facilityName'] = $this->facilities->getName($facilityId);
        $this->layout->view('attendances/facilities/report', $this->data);
    }

    public function viewAll()
    {
        $this->checkFacility();
        $this->load->model('facility_designation');
        $this->data['facilities'] = $this->facilities->getFullDetail($this->facilityId);

        if (empty($this->data['facilities'])) {
            $this->redirectForFailure('attendances/facilities', 'Something went wrong while getting facilities information.');
        }

        $this->data['facilityPosts'] = $this->facilitiesdesignations->getFacilityPosts($this->facilityId);
        $this->layout->view('attendances/facilities/view-all', $this->data);
    }

    public function edit($Id = null)
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForFacilityEdit();
        $this->load->model('location');

        $facilityId = $this->session->userdata('facilityId');

        if (empty($facilityId)) {
            $facilityId = $Id;
        }

        if (empty($_POST)) {
            if (!($this->data['facilities'] = $this->facilities->getDetail($facilityId))) {

                if ($this->session->userdata('facilityId')) {
                    $this->redirectForFailure('attendances/facilities/index', 'Something went wrong while getting facilities information.');
                } else {
                    $this->redirectForFailure('attendances/admin/viewFacilities', 'Something went wrong while getting facilities information.');
                }
            }

            $this->data['facilityId'] = $facilityId;

        } elseif ($this->form_validation->run()) {
            $this->facilities->modify($_POST, $facilityId);

            if ($this->session->userdata('facilityId')) {
                $this->redirectForSuccess('attendances/facilities/index', 'Data are updated successfully.');
            } else {
                $this->redirectForSuccess('attendances/admin/viewFacilities', 'Data are updated successfully.');
            }

        } else {
            $this->data['errorMessage'] = 'Please correct the following errors.';
            $this->data['facilities'] = $_POST;
        }

        $this->getLocations();
        $this->data['types'] = $this->facilities->getAllTypes();
        $this->layout->view('attendances/facilities/edit', $this->data);
    }      

    public function editPassword()
    {
        $this->checkFacility();
        $this->load->model('user');
        $this->load->library('form_validation');
        $this->form_validation->setRulesForPasswordEditByFacility();

        if (!empty($_POST)) {

            if ($this->form_validation->run()) {
                $this->users->changePassword($_POST, $this->facilityId);
                $this->redirectForSuccess('attendances/facilities', 'Password is updated successfully');
            } else {
                $this->data['errorMessage'] = 'Please check the following errors.';
            }
        }

        $this->data['facilityName'] = $this->facilities->getName($this->facilityId);
        $this->layout->view('attendances/facilities/change-password', $this->data);
    }    
    

    protected function getLocations()
    {
        $this->data['divisions'] = $this->locations->getAllDivisions();
        $this->data['districts'] = $this->locations->getAllDistricts();
        $this->data['upazillas'] = $this->locations->getAllUpazillas();
    }

    public function validate_password($password)
    {
        if ($this->users->validatePassword(array(
            'password' => $password,
            'type' => 'user',
            'facility_id' => $this->facilityId
           ))) {
            return true;
        } else {
            $this->form_validation->set_message(
                'validate_password',
                'Password is not correct.');
            return false;
        }
    }

    protected function checkFacility()
    {
        if (!$this->session->userdata('facilityId')) {
            $this->redirectForFailure('attendances/users/index', 'Something went wrong.');
        }

        $this->facilityId = $this->session->userdata('facilityId');
    }
}