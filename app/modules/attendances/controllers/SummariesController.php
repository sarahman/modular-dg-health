<?php

/**
 * Description of Summaries Controller
 *
 * @package     Controller
 * @author      Eftakhairul
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class SummariesController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->ensureLoggedIn();

        $this->load->library('pagination');
        $this->load->model('summary');
        $this->load->model('location');
        $this->load->model('facility');
        $this->load->model('attendance');
        $this->load->helper('math');
    }
    
    public function index()
    {
        $this->load->helper('date');
        $page = 0;
        $uri = site_url('attendances/summaries/index');

        $uriAssoc = $this->uri->uri_to_assoc();

        if (!empty($_POST)) {

            $_POST['startingDate'] = human_to_mysql($this->input->post('startingDate'));
            $_POST['endingDate'] = human_to_mysql($this->input->post('endingDate'));

            $this->load->helper('refactor');
            $_POST = eliminateNullField($_POST);
            $uri .= $this->uri->assoc_to_uri($_POST);
            redirect($uri);
        }

        else if (!empty($uriAssoc)) {
            $page = empty($uriAssoc['page']) ? 0 : $uriAssoc['page'];
            unset($uriAssoc['page']);

            $uri .= $this->uri->assoc_to_uri($uriAssoc);
        }
        
        $this->data['facilities'] = $this->summaries->findEmptyFacilities($uriAssoc, $page);
        $this->data['filters'] = $uriAssoc;

        $paginationOptions = array(
            'baseUrl' => $uri . '/page/',
            'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
            'numRows' => $this->summaries->countRows($uriAssoc)
        );

        $this->pagination->setOptions($paginationOptions);

        $this->layout->view('attendances/summaries/empty-facility', $this->data);
    }

    public function viewPost()
    {
        $uri = site_url('attendances/summaries/viewPost');

        $this->initialization($uri, $_POST);
        $this->data['date'] = '';

        $this->layout->view('attendances/summaries/post', $this->data);
    }
    
    public function sumViewPost()
    {
        $this->data['facilities'] = $this->summaries->sumViewPost();
        $this->layout->view('attendances/summaries/sum-view-post', $this->data);
    }

    public function typeViewPost()
    {
        $this->data['types'] = $this->getFacilityTypes();
        $this->data['showType'] = true;

        $this->data['facilities'] = $this->summaries->sumViewPost($_POST);
        $this->layout->view('attendances/summaries/sum-view-post', $this->data);
    }

    public function viewAbsence()
    {
        $this->load->helper('date');

        $this->data['showDate'] = true;
        $url = site_url('attendances/summaries/viewAbsence');
        $page = 0;
        $uriAssoc = $this->uri->uri_to_assoc();
        $this->data['dateRange'] = '';

        if (!empty($_POST)) {

            $_POST['startingDate'] = human_to_mysql($this->input->post('startingDate'));
            $_POST['endingDate'] = human_to_mysql($this->input->post('endingDate'));

            $this->data['dateRange'] = $this->input->post('startingDate') . '/' . $this->input->post('endingDate');

            $this->load->helper('refactor');
            $data = eliminateNullField($_POST);

            $url .= $this->uri->assoc_to_uri($data);
            redirect($url);
        }

        else if (!empty($uriAssoc)) {
            $page = empty($uriAssoc['page']) ? 0 : $uriAssoc['page'];
            unset($uriAssoc['page']);

            $url .= $this->uri->assoc_to_uri($uriAssoc);
        }

        $this->getLocations();
        $this->data['facilities'] = $this->summaries->absentStatus($uriAssoc, $page);
        
        $this->data['filters'] = $uriAssoc;

        $paginationOptions = array(
            'baseUrl' => $url . '/page/',
            'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
            'numRows' => $this->summaries->countRowsOfAbsence($uriAssoc)
        );

        $this->pagination->setOptions($paginationOptions);
        
        $this->layout->view('attendances/summaries/view-absences', $this->data);
    }
    
    public function sumOfAbsences()
    {
        $this->data['facilities'] = $this->summaries->sumOfAbsences($_POST);
        $this->layout->view('attendances/summaries/sum-of-absences', $this->data);
    }
    
    public function typeSumOfAbsences()
    {
        $this->data['showType'] = true;
        $this->data['types'] = $this->getFacilityTypes();

        $this->data['facilities'] = $this->summaries->sumOfAbsences($_POST);
        $this->layout->view('attendances/summaries/sum-of-absences', $this->data);
    }

    private function initialization($url, $data = array())
    {
        $page = 0;
        $uriAssoc = $this->uri->uri_to_assoc();

        if (!empty($data)) {
            if (!empty($data['date'])) {
                $this->load->helper('date');
                $data['date'] = human_to_mysql($data['date']);
            }

            $this->load->helper('refactor');
            $data = eliminateNullField($data);

            $url .= $this->uri->assoc_to_uri($data);
            redirect($url);
        }

        else if (!empty($uriAssoc)) {
            $page = empty($uriAssoc['page']) ? 0 : $uriAssoc['page'];
            unset($uriAssoc['page']);

            $url .= $this->uri->assoc_to_uri($uriAssoc);
        }

        $this->getLocations();
        $this->data['facilities'] = $this->attendances->search($uriAssoc, $page);
        $this->data['filters'] = $uriAssoc;

        $paginationOptions = array(
            'baseUrl' => $url . '/page/',
            'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
            'numRows' => $this->facilities->countRows($uriAssoc)
        );

        $this->pagination->setOptions($paginationOptions);
    }

    private function getLocations()
    {
        $this->data['divisions'] = $this->locations->getAllDivisions();
        $this->data['districts'] = $this->locations->getAllDistricts();
        $this->data['upazillas'] = $this->locations->getAllUpazillas();
    }

    private function getFacilityTypes()
    {
        return $this->facilities->getAllTypes();
    }
}