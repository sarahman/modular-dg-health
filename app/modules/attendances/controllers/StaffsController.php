<?php

/**
 * Staff Controller
 *
 * @package     Controller
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class StaffsController extends BaseController
{
    protected $facilityId;

    public function __construct()
    {
        parent::__construct();
        $this->checkFacility();
        $this->load->model('staff');
        $this->load->model('facility');
        $this->load->model('department');
        $this->load->model('category');
        $this->load->model('designation');
    }

    public function index()
    {
        $this->data['staffs'] = $this->staffs->getAll($this->facilityId);
        $this->data['facilityName'] = $this->facilities->getName($this->facilityId);
        $this->layout->view('attendances/staffs/index', $this->data);
    }

    public function add()
    {
        $this->load->model('facility_designation');
        $this->load->library('form_validation');
        $this->form_validation->setRulesForStaffEntry();

        if (!empty($_POST)) {

            if ($this->form_validation->run()) {
                $data = array(
                    'facility_id' => $this->facilityId,
                    'department_id' => $this->input->post('department_id'),
                    'designation_id' => $this->input->post('designation_id')
                );
                
                if ($this->facilitiesdesignations->checkVacancy($data)) {
                    $_POST['facility_id'] = $this->facilityId;
                    $_POST['status'] = 0;
                    $this->staffs->save($_POST);
                    $this->facilities->incrementDoctorNumber($this->facilityId);
                    $this->facilitiesdesignations->incrementFilledUpPostNumber($data);
                    $this->redirectForSuccess('attendances/facilities',
                           'The doctor information is successfully inserted.');
                } else {
                    $this->data['errorMessage'] = 'No vacancy is available.';
                }
            } else {
                $this->data['errorMessage'] = 'Please correct the following errors.';
            }

        }

        $this->getNecessaryData();
        $this->data['facilityName'] = $this->facilities->getName($this->facilityId);
        $this->layout->view('attendances/staffs/add', $this->data);
    }

    public function view()
    {
        $staffId = $this->uri->segment(3);
        $this->data['staffs'] = $this->staffs->getStaffProfile($staffId);
        if (empty($this->data['staffs'])) {
            $this->redirectForFailure('attendances/staffs',
                   'Something went wrong while getting staffs information.');
        }

        $religions = $this->config->item('religion');
        $statuses = $this->config->item('marital_status');
        $classes = $this->config->item('job_class');
        $this->data['staffs']['sex'] = ($this->data['staffs']['sex'] === 1) ? 'Male' : 'Female';
        $this->data['staffs']['marital_status'] = $statuses[$this->data['staffs']['marital_status']];
        $this->data['staffs']['job_class'] = $classes[$this->data['staffs']['job_class']];
        $this->data['staffs']['religion'] = $religions[$this->data['staffs']['religion']];
        $this->layout->view('attendances/staffs/view', $this->data);
    }

    public function edit()
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForStaffEntry();

        if (empty($_POST)) {
            $staffId = $this->uri->segment(3);
            $this->data['staffs'] = $this->staffs->getStaffDetailsById($staffId);
            if (empty($this->data['staffs'])) {
                $this->redirectForFailure('attendances/staffs',
                       'Something went wrong while getting staffs information.');
            }

        } else {

            if ($this->form_validation->run()) {

                $this->staffs->update($_POST, $this->input->post('staff_id'));
                $this->redirectForSuccess('attendances/staffs',
                       'The doctor information is successfully inserted.');
            } else {
                $this->data['errorMessage'] = 'Please correct the following errors.';
                $this->data['staffs'] = $_POST;
            }
        }

        $this->getNecessaryData();
        $this->data['facilityName'] = $this->facilities->getName($this->facilityId);
        $this->layout->view('attendances/staffs/edit', $this->data);
    }

    public function viewUnavailableStaffs()
    {
        $this->data['unavailableStaffs'] = $this->staffs->getAllUnavailableStaffs($this->facilityId);
        if (empty($this->data['unavailableStaffs'])) {
            $this->redirectForFailure('attendances/facilities',
                   'Something went wrong while getting facilities information.');
        }

        $this->data['facilityName'] = $this->facilities->getName($this->facilityId);
        $this->data['events'] = $this->config->item('event');
        $this->layout->view('attendances/staffs/view-unavailable-staffs', $this->data);
    }

    public function authorizeAbsences ()
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForAuthorizeAbsentEvent();
        $this->load->model('authorized_absence');

        if (!empty($_POST)) {

            if ($this->form_validation->run()) {

                    $data = array("date_to" => $_POST['date'],
                                  "staff_id" => $_POST['staff_id'],
                                  "facility_id" => $this->facilityId
                        );

                    $this->authorizeabsences->save($data);
                    echo 'true';                    
                    return;

            } else {
                $this->data['errorMessage'] = 'Please check the following errors.';
            }
        }

         $this->load->view('attendances/staffs/authorize-absences', $this->data);
    }

    protected function checkFacility()
    {
        if (!$this->session->userdata('facilityId')) {
            $this->redirectForFailure('attendances/users/index', 'Something went wrong.');
        }

        $this->facilityId = $this->session->userdata('facilityId');
    }

    protected function getNecessaryData()
    {
        $this->data['departments'] = $this->departments->getAll();
        $this->data['designations'] = $this->designations->getAll();
        $this->data['categories'] = $this->categories->getAll();
    }
}