<?php

/**
 * Description of Search Controller
 * 
 * @author      Eftakhairul
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class SearchController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->ensureLoggedIn();
        $this->load->model('location');
        $this->load->model('facility');
        $this->load->model('attendance');
        $this->load->model('absence');
        $this->load->library('pagination');
    }

    public function index()
    {
        $this->load->helper('date');

        $uri = site_url('attendances/search/index');
        $this->initialize($uri, $_POST);
        $this->data['dateRange'] = '';

        $this->layout->view('attendances/search/index', $this->data);
    }

    public function byDate()
    {
        $uri = site_url('attendances/search/byDate');
        $this->data['dateRange'] = '';
        $this->initialize($uri, $_POST);

        if (!empty($this->data['filters']['startingDate'])) {

            $this->load->helper('date');
            $this->data['dateRange'] = 'startingDate/' . human_to_mysql($this->data['filters']['startingDate'])
                    . '/endingDate/' . human_to_mysql($this->data['filters']['endingDate']);
        }

        $this->data['showDate'] = true;
        $this->layout->view('attendances/search/index', $this->data);
    }

    public function byType()
    {
        $uri = site_url('attendances/search/byType');
        $this->data['dateRange'] = '';
        $this->initialize($uri, $_POST);

        $this->data['showType'] = true;
        $this->data['types'] = $this->facilities->getAllTypes();

        $this->layout->view('attendances/search/index', $this->data);
    }

    public function searchFacilityByID()
    {
        $page = 0;
        $uriAssoc = $this->uri->uri_to_assoc();
        $url = site_url('attendances/search/searchFacilityByID');

        if (!empty($uriAssoc['facilityId'])) {

            if (!empty($uriAssoc['page'])) {
                $page = $uriAssoc['page'];
                unset($uriAssoc['page']);
                }

            $this->data['facilities'] = $this->absences->searchByID($uriAssoc, $page);
            $this->data['facilityName'] = $this->facilities->getName($uriAssoc['facilityId']);

            $url .= $this->uri->assoc_to_uri($uriAssoc);     

            $paginationOptions = array(
                'baseUrl' => $url. '/page/',
                'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
                'numRows' => $this->absences->countRows($uriAssoc)
            );
            $this->pagination->setOptions($paginationOptions);
        } else {
            $this->data['error'] = "Sorry, information is not found.";
        }

        $this->layout->view('attendances/search/facilities', $this->data);
    }

    private function initialize($url, $data = array())
    {
        $page = 0;

        $uriAssoc = $this->uri->uri_to_assoc();

        if (!empty($data)) {

            $this->load->helper('refactor');
            $data = eliminateNullField($data);
            $url .= $this->uri->assoc_to_uri($data);
            redirect($url);
        }

        else if (!empty($uriAssoc)) {
            $page = empty($uriAssoc['page']) ? 0 : $uriAssoc['page'];
            unset($uriAssoc['page']);

            $url .= $this->uri->assoc_to_uri($uriAssoc);
        }

        $this->getLocations();
        $this->data['facilities'] = $this->attendances->search($uriAssoc, $page);
        $this->data['filters'] = $uriAssoc;

        $paginationOptions = array(
            'baseUrl' => $url . '/page/',
            'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
            'numRows' => $this->facilities->countRows($uriAssoc)
        );

        $this->pagination->setOptions($paginationOptions);
    }
    
    public function getDoctorRecord($doctorId)
    {
        $this->load->model('staff');
        
        if (!empty($doctorId)) {
            $this->data['records'] = $this->absences->getRecordById($doctorId);
            $this->data['doctor'] = $this->staffs->getDoctorNameById($doctorId);
        } else {
            $this->data['error'] = "Sorry, information is not found.";
        }
        
        $this->layout->view('attendances/search/doctor-detail', $this->data);
    }

    private function getLocations()
    {
        $this->data['divisions'] = $this->locations->getAllDivisions();
        $this->data['districts'] = $this->locations->getAllDistricts();
        $this->data['upazillas'] = $this->locations->getAllUpazillas();
    }
}