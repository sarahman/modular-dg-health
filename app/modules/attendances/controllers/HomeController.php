<?php

/**
 * Description of Home Controller
 *
 * @author      eftakhairul
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class HomeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->ensureLoggedIn();
        
        $this->load->model('location');
        $this->load->model('facility');
        $this->load->model('attendance');
        $this->load->library('pagination');
    }

    public function index()
    {
        $this->load->helper('date');
        $url = site_url('attendances/home/index');
        $page = 0;
        $uriAssoc = $this->uri->uri_to_assoc();

        if (!empty($_POST)) {
            if (!empty($_POST['date'])) {
                $_POST['date'] = human_to_mysql($_POST['date']);
            }

            $this->load->helper('refactor');
            $_POST = eliminateNullField($_POST);

            $url .= $this->uri->assoc_to_uri($_POST);
            redirect($url);
        }

        else if (!empty($uriAssoc)) {
            $page = empty($uriAssoc['page']) ? 0 : $uriAssoc['page'];
            unset($uriAssoc['page']);

            $url .= $this->uri->assoc_to_uri($uriAssoc);
        }

        $this->_getLocations();
        $this->data['facilities'] = $this->facilities->getAllFacilities();
        $this->data['facilitiesFound'] = $this->attendances->searchForGraph($uriAssoc, $page);
        $this->data['filters'] = $uriAssoc;

        $paginationOptions = array(
            'baseUrl' => $url . '/page/',
            'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
            'numRows' => $this->attendances->countRows($uriAssoc)
        );

        $this->pagination->setOptions($paginationOptions);

        $this->layout->view('attendances/home/index', $this->data);
    }

    protected function _getLocations()
    {
        $this->data['divisions'] = $this->locations->getAllDivisions();
        $this->data['districts'] = $this->locations->getAllDistricts();
        $this->data['upazillas'] = $this->locations->getAllUpazillas();
    }
}