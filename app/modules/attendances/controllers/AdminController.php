<?php

/**
 * Description of Admin Controller
 *
 * @package     Controller
 * @author      Eftakhairul
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
class AdminController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user');
        $this->load->model('facility');
    }

    public function index()
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForSignIn();

        if (!empty($_POST)) {
            if ($this->form_validation->run()) {

                $_POST['type'] = 'admin';
                $result = $this->users->validateUser($_POST);

                if ($result) {

                    $this->session->set_userdata('username', $result['username']);
                    $this->session->set_userdata('userType', $result['type']);

                    $this->redirectForSuccess('attendances/home', 'You have successfully logged in.');

                } else {
                    $this->data['error'] = 'Enter correct Username & Password.';
                }

            } else {
                $this->data['error'] = 'Enter required information.';
            }
        }
        $this->load->view('attendances/admin/index', $this->data);
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('userType');
        $this->session->sess_destroy();
        redirect('admin');
    }

    public function viewFacilities()
    {
        $url = site_url('attendances/admin/viewFacilities/page/');
        $this->processPagination($url);
        
        $this->layout->view('attendances/admin/view-facilities', $this->data);
    }

    public function addFacility()
    {
        $this->load->model('facility');
        $this->load->model('location');
        $this->load->library('form_validation');
        $this->form_validation->setRulesForFacilityEntry();

        if (!empty($_POST)) {
            if ($this->form_validation->run()) {
                $this->load->model('facility');
                $facilityId = $this->facilities->save($_POST);
                $this->users->save(array_merge($_POST, array(
                        'facility_id' => $facilityId,
                        'type' => 'user'))
                );
                
                $this->redirectForSuccess('attendances/admin/viewFacilities',
                       'Facility information is successfully inserted.');
            } else {
                $this->data['errorMessage'] = 'Please correct the following errors.';
            }
        }

        $this->getLocations();
        $this->data['types'] = $this->facilities->getAllTypes();
        
        $this->layout->view('attendances/admin/add-facilities', $this->data);
    }
    
    public function reportSanctionedPost() 
    {
        $url = site_url('attendances/admin/reportSanctionedPost/page/');
        $this->processPagination($url);

        $this->layout->view('attendances/admin/report-sanctioned-post', $this->data);
    }

    public function getReportByFacilityId($facilityId)
    {
        $this->load->model('facility_designation');

        if (!empty($facilityId)) {
            $this->data['facilityName'] = $this->facilities->getName($facilityId); 
            $this->data['postDetails'] = $this->facilitiesdesignations->getReportByFacilityId($facilityId);
        }

        $this->layout->view('attendances/admin/get-report-by-facility', $this->data);
    }

    protected function getLocations()
    {
        $this->data['divisions'] = $this->locations->getAllDivisions();
        $this->data['districts'] = $this->locations->getAllDistricts();
        $this->data['upazillas'] = $this->locations->getAllUpazillas();
    }

    public function editPassword($facilityId)
    {        
        $this->load->library('form_validation');
        $this->form_validation->setRulesEditPasswordEntry();

        if (!empty($_POST)) {

            if ($this->form_validation->run()) {
                $this->users->changePassword($_POST, $facilityId);
                $this->redirectForSuccess('attendances/admin/viewFacilities', 'Password is updated successfully');
            } else {
                $this->data['errorMessage'] = 'Please check the following errors.';
            }
        }

        $this->data['facilityName'] = $this->facilities->getName($facilityId);
        $this->layout->view('attendances/admin/change-password', $this->data);
    }

    public function addSanctionPost($facilityId)
    {
        $this->load->model('facility_designation');

        $this->data['facilities'] = $this->facilities->getNameId($facilityId);
        $this->data['posts'] = $this->facilitiesdesignations->getFacilityPostsDetail($facilityId);

        $this->layout->view('attendances/admin/add-sanction-post', $this->data);
    }

    public function absentEvent() 
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForAbsentEvent();

        if (!empty($_POST)) {

            if ($this->form_validation->run()) {

                $data = $_POST['eventBox']."|".$_POST['comment']."|".$_POST['date'];

                echo 'true';
                echo $data;
                return;

            } else {
                $this->data['errorMessage'] = 'Please check the following errors.';
            }
        }
        
        $this->load->view('attendances/admin/absent-event');
    }

    public function addPost($facilityId = null)
    {
        $this->load->library('form_validation');
        $this->form_validation->setRulesForSanctionPost();

        if (!empty($_POST)) {

            if ($this->form_validation->run()) {
                $this->load->model('facility_designation');
                if (!$this->facilitiesdesignations->checkDataEntered($_POST)) {
                    $insertId = $this->facilitiesdesignations->save($_POST);
                    echo 'true';
                    echo $insertId;
                    return;
                } else {
                    $this->data['errorMessage'] = 'This record is already entered.';
                }
            } else {
                $this->data['errorMessage'] = 'Please check the following errors.';
            }
        }

        $this->getNecessaryData();
        //$this->data['facility_id'] = $facilityId;
        $this->load->view('attendances/admin/add-post', $this->data);
    }

    public function editPost()
    {
        $this->load->model('facility_designation');
        $this->load->library('form_validation');
        $this->form_validation->setRulesForSanctionPost();

        if (empty($_POST)) {
            $rowId = $this->uri->segment(3);
            if (empty($rowId)) {
                return;
            }

            $this->data['sanctionedPost'] = $this->facilitiesdesignations->
                                        getSanctionedPostRow($rowId);
        } else if ($this->form_validation->run()) {
            $insertId = $this->facilitiesdesignations->modify($_POST, $this->input->post('facility_designation_id'));
            echo 'true';
            echo $insertId;
            return;
        } else {
            $this->data['errorMessage'] = 'Please check the following errors.';
            $this->data['sanctionedPost'] = $_POST;
        }

        $this->getNecessaryData();
        $this->load->view('attendances/admin/edit-post', $this->data);
    }
    
    private function processPagination($url)
    {
        $this->load->library('pagination');

        $uriAssoc = $this->uri->uri_to_assoc();
        $page = empty($uriAssoc['page']) ? 0 : $uriAssoc['page'];
        $this->data['facilities'] = $this->facilities->getAllFacilities($page);

        $paginationOptions = array(
            'baseUrl' => $url,
            'segmentValue' => $this->uri->getSegmentIndex('page') + 1,
            'numRows' => $this->facilities->countAllFacility()
        );

        $this->pagination->setOptions($paginationOptions);
    }

    protected function getNecessaryData()
    {
        $this->load->model('department');
        $this->load->model('category');
        $this->load->model('designation');
        $this->data['departments'] = $this->departments->getAll();
        $this->data['designations'] = $this->designations->getAll();
        $this->data['categories'] = $this->categories->getAll();
    }
}
