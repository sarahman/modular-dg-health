<?php

include_once APPPATH . 'libraries/MX_Form_validation'.EXT;
class MY_Form_validation extends MX_Form_validation
{
    public function setRulesForAuthorizeAbsentEvent()
    {
        $config = array(
            array(
                'field' => 'date',
                'label' => 'Date',
                'rules' => 'required'
            )
        );

        $this->set_rules($config);
    }

    public function setRulesForAbsentEvent()
    {
        $config = array(
            array(
                'field' => 'eventBox',
                'label' => 'Event',
                'rules' => 'required'
            ),
            array(
                'field' => 'comment',
                'label' => 'Comment',
                'rules' => 'required'
            ),
            array(
                'field' => 'date',
                'label' => 'Date',
                'rules' => 'required'
            )
        );

        $this->set_rules($config);
    }

    public function setRulesForSignIn()
    {
        $config = array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[6]'
            )
        );

        $this->set_rules($config);
    }

    public function setRulesEditPasswordEntry()
    {
        $config = array(
            array(
                'field'   => 'password',
                'label'   => 'Password',
                'rules'   => 'required|min_length[6]'
            ),
            array(
                'field'   => 'confirmedPassword',
                'label'   => 'Confirmation Password',
                'rules'   => 'required|matches[password]'
            )
        );

        $this->set_rules($config);
    }

    public function setRulesForPasswordEditByFacility()
    {
        $config = array(
            array(
                'field'   => 'current_password',
                'label'   => 'Current Password',
                'rules'   => 'required|min_length[6]|callback_validate_password'
            ),
            array(
                'field'   => 'new_password',
                'label'   => 'New Password',
                'rules'   => 'required|min_length[6]'
            ),
            array(
                'field'   => 'confirmedPassword',
                'label'   => 'Confirmation Password',
                'rules'   => 'required|matches[new_password]'
            )
        );

        $this->set_rules($config);
    }

    public function setRulesForStaffEntry()
    {
        $config = array(
            array(
                'field'   => 'name',
                'label'   => 'Staff Name',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'category_id',
                'label'   => 'Category',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'department_id',
                'label'   => 'Department / Section',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'designation_id',
                'label'   => 'Designation',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'designation_status',
                'label'   => 'Designation Status',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'job_class',
                'label'   => 'Job Class',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'father_name',
                'label'   => 'Father Name',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'mother_name',
                'label'   => 'Mother Name',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'sex',
                'label'   => 'Sex',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'marital_status',
                'label'   => 'Marital status',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'religion',
                'label'   => 'Religion',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'birth_date',
                'label'   => 'Date of Birth',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'contact_no',
                'label'   => 'Contact no.',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'joining_date',
                'label'   => 'Joining date',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'joining_date_govt',
                'label'   => 'Joining to Govt. Organization',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'joining_date_position',
                'label'   => 'Joining to this position',
                'rules'   => 'required'
            ));

        $this->set_rules($config);
    }

    public function setRulesForSanctionPost()
    {
        $config = array(
            array(
                'field'   => 'category_id',
                'label'   => 'Category',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'department_id',
                'label'   => 'Department',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'designation_id',
                'label'   => 'Designation',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'no_of_sanctioned_posts',
                'label'   => 'Sanctioned Post',
                'rules'   => 'required'
            ));

        $this->set_rules($config);
    }

    public function setRulesForFacilityEntry()
    {
        $config = array(
            array(
                'field'   => 'name',
                'label'   => 'Facility Name',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'division_id',
                'label'   => 'Division',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'district_id',
                'label'   => 'District',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'upazilla_id',
                'label'   => 'Upazilla',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'type_id',
                'label'   => 'Facility Type',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'username',
                'label'   => 'Facility Username',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'password',
                'label'   => 'Password',
                'rules'   => 'required|min_length[6]'
            ),
            array(
                'field'   => 'confirmedPassword',
                'label'   => 'Confirmed Password',
                'rules'   => 'required|matches[password]'
            ));

        $this->set_rules($config);
    }

    public function setRulesForFacilityEdit()
    {
        $config = array(
            array(
                'field'   => 'name',
                'label'   => 'Facility Name',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'chairperson',
                'label'   => 'Chairperson Name',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'division_id',
                'label'   => 'Division',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'district_id',
                'label'   => 'District',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'upazilla_id',
                'label'   => 'Upazilla',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'type_id',
                'label'   => 'Facility Type',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'email_address',
                'label'   => 'Email Address',
                'rules'   => 'required|valid_email'
            ),
            array(
                'field'   => 'contact_no',
                'label'   => 'Contact No.',
                'rules'   => 'required'
            ));

        $this->set_rules($config);
    }
}