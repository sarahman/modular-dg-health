<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

set_include_path(get_include_path() .PATH_SEPARATOR . 'Classes/');
include_once 'PHPExcel/IOFactory.php';
 
class ExcelReader {

    private $objPHPExcel, $aSheet;

    public function __construct() {

        $this->objPHPExcel = PHPExcel_IOFactory::load('new1.xls');
        $this->objPHPExcel->setActiveSheetIndex(0);
        $this->aSheet = $this->objPHPExcel->getActiveSheet();
    }

    public function getAllRow()
    {
        $data = array();
        foreach($this->aSheet->getRowIterator() as $row)
        {
            $data[] = $row;
        }
        
        return $data;
    }

    public function getAllCell()
    {
        $data = array();
        foreach($this->aSheet->getRowIterator() as $key => $row) {

            $cellIterator = $row->getCellIterator();

            foreach($cellIterator as $cell) {
            $data[$key][] = $cell->getCalculatedValue();
             }
        }

        return $data;
    }
    
}
