<?php

/**
 * Base Controller
 *
 * Common tasks of all controllers are done here. Only inherit, no direct use please.
 *
 * @package     Base
 * @category    Controller
 * @author      Raju Mazumder <rajuniit@gmail.com>
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */
abstract class BaseController extends MX_Controller
{
    protected $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->prepareEnvironment();
        $this->populateFlashData();
    }

    protected function prepareEnvironment()
    {
        $this->load->library('Layout');
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }

    protected function populateFlashData()
    {
        $notify['message'] = $this->session->flashdata('message');
        $notify['messageType'] = $this->session->flashdata('messageType');

        $this->data['notification'] = $notify;
    }

    protected function ensureLoggedIn()
    {
        if (!$this->session->userdata('username')) {
            redirect('schedules/users');
        }
    }

    protected function redirectForSuccess($redirectLink, $message)
    {
        $this->session->set_flashdata('message', $message);
        $this->session->set_flashdata('messageType', 'success');
        redirect($redirectLink);
    }

    protected function redirectForFailure($redirectLink, $message)
    {
        $this->session->set_flashdata('message', $message);
        $this->session->set_flashdata('messageType', 'errormsg');
        redirect($redirectLink);
    }
}