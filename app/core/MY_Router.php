<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/*
 * MY_Router
 *
 * Extended the core CI_Router class in order to force a different naming
 * convention for controllers.
 *
 */

///* load the MX_Router class */
require APPPATH . "third_party/MX/Router.php";
class MY_Router extends MX_Router
{
}