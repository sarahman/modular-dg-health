<?php

class MY_URI extends CI_URI
{
    public function getSegmentIndex($segmentValue)
    {
        $keyArray = array_keys($this->segment_array(), $segmentValue);
        if (empty ($keyArray[0])) {
            return -1;
        }
        return $keyArray[0];
    }

    public function assoc_to_uri($data)
    {
        return '/' . parent::assoc_to_uri($data);
    }

    public function uri_to_assoc($n = 4, $default = array())
    {
        return $this->_uri_to_assoc($n, $default, 'segment');
    }
}