<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout
{
    var $registry;
    var $layout;

    public function Layout($layout = "layouts/main")
    {
        $this->obj = & get_instance();
        $this->layout = $layout;
    }

    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    public function view($view, $data=null, $return=false)
    {
        $view = $this->filterView($view);
        $data['content_for_layout'] = $this->obj->load->view($view,$data,true);

        if ($return)
        {
            $output = $this->obj->load->view($this->layout,$data, true);
            return $output;
        }
        else
        {
            $this->obj->load->view($this->layout,$data, false);
        }
    }

    protected function filterView($view)
    {
        $segments = explode('/', $view);
        $segmentsCount = count($segments);

        if ($segmentsCount == 1) {
            $view = $segments[0].'/'.$segments[0].'/'.$segments[0];
        } elseif ($segmentsCount == 2) {
            $view = $segments[0].'/'.$segments[0].'/'.$segments[1];
        }

        return $view;
    }
}