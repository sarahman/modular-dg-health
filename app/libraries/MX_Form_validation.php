<?php  defined('BASEPATH')  OR  exit('No direct script access allowed');
include_once BASEPATH . 'libraries/Form_Validation.php';
class MX_Form_validation extends CI_Form_validation
{
    public function __construct()
    {
        parent::__construct();
        $this->set_error_delimiters('','');
    }

    public function run($module = '', $group = '')
    {
        (is_object($module)) AND $this->CI =& $module;
        return parent::run($group);
    }
}